/*
 * Write the function strend(s,t), which returns 1 if the string t occurs at
 * the end of the string s, and zero otherwise.
 */

#include <stdio.h>
#include <string.h>

int
strend (char *s,
        char *t)
{
  if (strlen (s) < strlen (t))
    return 0;

  s += strlen (s) - strlen (t);

  for (size_t i = strlen (t); *s == *t && i; --i, ++t, ++s)
    ;
  if (!*t)
    return 1;

  return 0;
}

int
main (int   argc,
      char *argv[])
{
  if (strend ("Hello, world!", "world!"))
    printf ("strend: match\n");
  else
    printf ("strend: no match\n");

  return 0;
}
