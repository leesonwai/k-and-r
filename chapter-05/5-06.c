/*
 * Rewrite appropriate programs from earlier chapters and exercises with
 * pointers instead of array indexing. Good possibilities include getline
 * (Chapters 1 and 4), atoi, itoa, and their variants (Chapters 2, 3 and 4),
 * reverse (Chapter 3), and strindex and getop (Chapter 4).
 */

#include <stdio.h>
#include <string.h>

#define MAX 256

int
strindex (char *s,
          char *t)
{
  char *start = s;

  for (char *pt = t; *s; ++s)
    {
      for (char *ps = s; *ps == *pt && *pt; ++ps, ++pt)
        ;
      if (!*pt)
        return (s - start);
    }

  return -1;
}

int
getnline (char *s,
          int   lim)
{
  char *start = s;

  while ((*s++ = getchar ()) != '\n' && lim--)
    ;
  *--s = '\0';

  return (s - start);
}

void
reverse (char *s)
{
  char *end = s + strlen (s) - 1;

  while (s < end)
    {
      int tmp = *s;
      *s++ = *end;
      *end-- = tmp;
    }
}

int
main (int   argc,
      char *argv[])
{
  char line[MAX];

  if (getnline (line, MAX))
    {
      printf ("%s\n", line);
      reverse (line);
      printf ("%s\n", line);
    }

  printf ("strindex: %d\n", strindex ("Kernighan & Ritchie", "Ritchie"));

  return 0;
}
