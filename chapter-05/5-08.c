/*
 * There is no error checking in day_of_year or month_day. Remedy this defect.
 */

#include <stdio.h>

static char daytab[2][13] = {
  {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
  {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
};

int
month_day (unsigned int  year,
           unsigned int  yearday,
           int          *month,
           int          *day)
{
  int leap = (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
  int i;

  if (year <= 1752)
    return -1;
  if ((leap && yearday > 366) || (!leap && yearday > 365))
    return -1;

  for (i = 1; yearday > daytab[leap][i]; ++i)
    yearday -= daytab[leap][i];

  *month = i;
  *day = yearday;

  return 0;
}

int
day_of_year (unsigned int year,
             unsigned int month,
             unsigned int day)
{
  int leap = (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;

  if (year <= 1752 || month > 12)
    return -1;
  if ((leap && day > 366) || (!leap && day > 365))
    return -1;

  for (int i = 1; i < month; ++i)
    day += daytab[leap][i];

  return day;
}

int
main (int   argc,
      char *argv[])
{
  int yearday;
  int month;
  int day;

  if ((yearday = day_of_year (1941, 9, 9)) > 0)
    printf ("day_of_year (1941, 9, 9): %d\n", yearday);
  if ((month_day (1941, yearday, &month, &day)) != -1)
    printf ("month_day (1941, %d, ...): %d %d\n", yearday, month, day);

  return 0;
}
