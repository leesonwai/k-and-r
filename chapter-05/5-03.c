/*
 * Write a pointer version of the function strcat that we showed in Chapter 2.
 * strcat(s,t) copies the string t to the end of s.
 */

#include <stdio.h>

#define MAX 256

void
strcat (char *s,
        char *t)
{
  while (*s)
    ++s;
  while (*s++ = *t++)
    ;
}

int
main (int   argc,
      char *argv[])
{
  char s[MAX] = "Kernighan";

  strcat (s, " & Ritchie");
  printf ("%s\n", s);

  return 0;
}
