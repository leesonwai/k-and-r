/*
 * Add a field-handling capability, so sorting may be done on fields within
 * lines, each field sorted according to an independent set of options. (The
 * index for this book was sorted with -df for the index category and -n for the
 * page numbers.)
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFSIZE 4096
#define DELIM ' '
#define ISANB(c) isalnum (c) || isblank (c)
#define MAXLEN 128
#define MAXLINES 256
#define NUM '0'

enum
{
  DIR = 1,
  FOLD = 2,
  NUMERIC = 4,
  REVERSE = 8
};

int bufp;
int field;
char *lines[MAXLINES];

void
getfield (char *f,
          char *s)
{
  for (int i = 0; i < field && *s; ++s)
    {
      if (*s == DELIM)
        ++i;
    }
  while (*s != DELIM && *s)
    *f++ = *s++;
  *f = '\0';
}

void
swap (void *v[],
      int   i,
      int   j)
{
  void *tmp;

  tmp = v[i];
  v[i] = v[j];
  v[j] = tmp;
}

int
getnline (char *s,
          int   lim)
{
  int c;
  char *start = s;

  while ((c = getchar ()) != '\n' && --lim)
    {
      if (c == EOF)
        return EOF;
      *s++ = c;
    }
  *s = '\0';

  return (s - start);
}

void
writelines (char *lines[],
            int   nlines)
{
  while (nlines--)
    printf ("%s\n", *lines++);
}

void
reverse (void *lines[],
         int   j)
{
  void *tmp;

  for (int i = 0; i < j; ++i, --j)
    {
      tmp = lines[i];
      lines[i] = lines[j];
      lines[j] = tmp;
    }
}

int
strfcmp (const char *s1,
         const char *s2)
{
  while (*s1 && *s1 == *s2)
    {
      ++s1;
      ++s2;
    }

  return (!*s2) ? 0 : (*s1 - *s2);
}


int
stricmp (const char *s1,
         const char *s2)
{
  while (*s1 && tolower (*s1) == tolower (*s2))
    {
      ++s1;
      ++s2;
    }

  return (!*s2) ? 0 : (tolower (*s1) - tolower (*s2));
}

int
strdicmp (const char *s1,
          const char *s2)
{
  while (*s1 && (tolower (*s1) == tolower (*s2) || !ISANB (*s1) || !ISANB (*s2)))
    {
      ++s1;
      ++s2;
    }

  return (!*s2) ? 0 : (tolower (*s1) - tolower (*s2));
}

int
strdcmp (const char *s1,
         const char *s2)
{
  while (*s1 && (*s1 == *s2 || !ISANB (*s1) || !ISANB (*s2)))
    {
      ++s1;
      ++s2;
    }

  return (!*s2) ? 0 : (*s1 - *s2);
}

int
numcmp (const char *s1,
        const char *s2)
{
  return (atof (s1) - atof (s2));
}

void
quicksort (void *v[],
           int   left,
           int   right,
           int   (*cmp) (void *, void *))
{
  int pivot;
  int last;

  if (left >= right)
    return;

  pivot = (left + right) / 2;
  swap (v, left, pivot);
  last = left;

  for (int i = left + 1; i <= right; ++i)
    {
      if (field)
        {
          char f1[MAXLEN];
          char f2[MAXLEN];

          getfield (f1, v[i]);
          getfield (f2, v[left]);
          if ((*cmp)(f1, f2) < 0)
            swap (v, ++last, i);
        }
      else
        {
          if ((*cmp) (v[i], v[left]) < 0)
            swap (v, ++last, i);
        }
    }

  swap (v, left, last);
  quicksort (v, left, last - 1, cmp);
  quicksort (v, last + 1, right, cmp);
}

int
readlines (char *buf,
           char *lines[],
           int   lim)
{
  int len;
  char s[MAXLEN];
  int nlines = 0;

  while ((len = getnline (s, MAXLEN)) != EOF && len > 0)
    {
      if (nlines >= MAXLINES || bufp + len >= BUFSIZE)
        {
          return -1;
        }
      else
        {
          strcpy (buf, s);
          lines[nlines++] = buf;
          buf += len + 1;
          bufp += len;
        }
    }

  return nlines;
}

int
getarg (char *arg)
{
  ++arg;
  while (isdigit (*arg))
    {
      if (!*++arg)
        return NUM;
    }

  return *arg;
}

int
main (int   argc,
      char *argv[])
{
  char flags = 0;
  char buf[BUFSIZE];
  int nlines;

  while (--argc && (*++argv)[0] == '-')
    {
      switch (getarg (*argv))
        {
        case NUM:
          field = abs (atoi (*argv));
          break;
        case 'd':
          flags |= DIR;
          break;
        case 'f':
          flags |= FOLD;
          break;
        case 'n':
          flags |= NUMERIC;
          break;
        case 'r':
          flags |= REVERSE;
          break;
        default:
          printf ("Invalid option '%s'\n", *argv);
          return 1;
        }
    }

  if ((nlines = readlines (buf, lines, MAXLINES)) > 0)
    {
      if (flags & DIR && !(flags & FOLD))
        quicksort ((void **) lines, 0, nlines - 1, (int (*) (void *, void *))
                  (flags & NUMERIC ? numcmp : strdcmp));
      else if (flags & DIR && flags & FOLD)
        quicksort ((void **) lines, 0, nlines - 1, (int (*) (void *, void *))
                  (flags & NUMERIC ? numcmp : strdicmp));
      else if (!(flags & DIR) && flags & FOLD)
        quicksort ((void **) lines, 0, nlines - 1, (int (*) (void *, void *))
                  (flags & NUMERIC ? numcmp : stricmp));
      else
        quicksort ((void **) lines, 0, nlines - 1, (int (*) (void *, void *))
                  (flags & NUMERIC ? numcmp : strfcmp));
      if (flags & REVERSE)
        reverse ((void **) lines, nlines - 1);

      writelines (lines, nlines);
    }

  return 0;
}
