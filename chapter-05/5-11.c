/*
 * Modify the programs entab and detab (written as exercises in Chapter 1) to
 * accept a list of tab stops as arguments. Use the default tab settings if
 * there are no arguments.
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEFAULT 8
#define MAX 256

void
entab (char *s,
       char *t,
       int  *tabs)
{
  int col;
  int rem;

  for (col = 1; (*s++ = *t++) && col < MAX; ++col)
    {
      for (rem = 0; isblank (*t); ++t, ++col, ++rem)
        {
          if (col == *tabs)
            {
              *s++ = '\t';
              ++tabs;
              rem = 0;
            }
          else if (*tabs == -1 && col % DEFAULT == 0)
            {
              *s++ = '\t';
              rem = 0;
            }
        }

      while (rem--)
        *s++ = ' ';
    }
  *s = '\0';
}

int
main (int   argc,
      char *argv[])
{
  int n;
  int i = 0;
  int tabs[MAX];
  char s[MAX];

  while (--argc && (n = atoi (*++argv)) && i < MAX - 2)
    tabs[i++] = n;
  tabs[i] = -1;

  if (argc != 1)
    {
      printf ("Usage: [TABSTOPS]... 'STRING'\n");
      return 1;
    }

  entab (s, *argv, tabs);
  printf ("%s\n", s);

  return 0;
}
