/*
 * Modify undcl so that it does not add redundant parentheses to declarations.
 */

#include <ctype.h>
#include <stdio.h>
#include <string.h>

#define BUFSIZE 32
#define MAXOUT 1024
#define MAXTMP 2048
#define MAXTOKEN 128

enum {
  BRACKETS,
  NAME,
  PARENS
};

int gettoken (void);

char buf[BUFSIZE];
int bufp;
char out[MAXOUT];
char token[MAXTOKEN];
int tokentype;

void
ungetch (int c)
{
  if (bufp >= BUFSIZE)
    printf ("error: too many characters\n");
  else
    buf[bufp++] = c;
}

int
getch ()
{
  return (bufp > 0) ? buf[--bufp] : getchar ();
}

int
undcl ()
{
  int type;
  char tmp[MAXTMP];

  while ((type = gettoken ()) != '\n')
    {
      if (type == BRACKETS || type == PARENS)
        {
          strcat (out, token);
        }
      else if (type == '*')
        {
          if ((type = gettoken ()) == BRACKETS || type == PARENS)
            sprintf (tmp, "(*%s)", out);
          else
            sprintf (tmp, "*%s", out);
          ungetch (type);
          strcpy (out, tmp);
        }
      else if (type == NAME)
        {
          sprintf (tmp, "%s %s", token, out);
          strcpy (out, tmp);
        }
      else
        {
          printf ("error: invalid input at '%s'\n", token);
          return 0;
        }
    }

  return 1;
}

int
gettoken ()
{
  int c;
  char *p = token;

  while ((c = getch ()) == ' ' || c == '\t')
    ;

  if (c == '(')
    {
      if ((c = getch ()) == ')')
        {
          strcpy (token, "()");
          return tokentype = PARENS;
        }
      else
        {
          ungetch (c);
          return tokentype = '(';
        }
    }
  else if (c == '[')
    {
      for (*p++ = c; (*p++ = getch ()) != ']'; )
        ;
      *p = '\0';
      return tokentype = BRACKETS;
    }
  else if (isalpha (c))
    {
      for (*p++ = c; isalnum (c = getch ()); )
        *p++ = c;
      *p = '\0';
      ungetch (c);
      return tokentype = NAME;
    }
  else
    {
      return tokentype = c;
    }
}

int
main (int   argc,
      char *argv[])
{
  while (gettoken () != EOF)
    {
      strcpy (out, token);
      if (undcl ())
        printf ("%s\n", out);
    }

  return 0;
}
