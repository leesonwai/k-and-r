/*
 * Write the program tail, which prints the last n lines of its input. By
 * default, n is 10, let us say, but it can be changed by an optional argument,
 * so that
 *
 *  tail -n
 *
 * prints the last n lines. The program should behave rationally no matter how
 * unreasonable the input or the value of n. Write the program so it makes the
 * best use of available storage; lines should be stored as in the sorting
 * program of Section 5.6, not in a two-dimensional array of fixed size.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFSIZE 4096
#define DEFAULT 10
#define MAXLEN 128
#define MAXLINES 256

int bufp;
char *lines[MAXLINES];

int
getnline (char *s,
          int   lim)
{
  int c;
  char *start = s;

  while ((c = getchar ()) != '\n' && --lim)
    {
      if (c == EOF)
        return EOF;
      *s++ = c;
    }
  *s = '\0';

  return (s - start);
}

void
writelines (char *lines[],
            int   nlines,
            int   tail)
{
  if (tail > nlines)
    tail = nlines;

  lines += nlines - tail;

  while (tail--)
    printf ("%s\n", *lines++);
}

int
readlines (char *buf,
           char *lines[],
           int   lim)
{
  int len;
  char s[MAXLEN];
  int nlines = 0;

  while ((len = getnline (s, MAXLEN)) != EOF && len > 0)
    {
      if (nlines >= MAXLINES || bufp + len >= BUFSIZE)
        {
          return -1;
        }
      else
        {
          strcpy (buf, s);
          lines[nlines++] = buf;
          buf += len + 1;
          bufp += len;
        }
    }

  return nlines;
}

int
main (int   argc,
      char *argv[])
{
  int n;
  int tail = DEFAULT;
  char buf[BUFSIZE];
  int nlines;

  if (argc > 2)
    {
      printf ("Usage: -[TAIL]\n");
      return 1;
    }
  else if (--argc && **++argv == '-' && (n = atoi (*argv)))
    {
      tail = abs (n);
    }

  if ((nlines = readlines (buf, lines, MAXLINES)) > 0)
    writelines (lines, nlines, tail);

  return 0;
}
