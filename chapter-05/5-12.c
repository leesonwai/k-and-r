/*
 * Extend entab and detab to accept the shorthand
 *
 *  entab -m +n
 *
 * to mean tab stops every n columns, starting at column m. Chosose convenient
 * (for the user) default behaviour.
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 256

void
entab (char *s,
       char *t,
       int   start,
       int   tabsize)
{
  int col;
  int rem;

  for (col = 1; (*s++ = *t++) && col < MAX; ++col)
    {
      if (col < start)
        continue;

      for (rem = 0; isblank (*t); ++t, ++col, ++rem)
        {
          if (col % tabsize == 0)
            {
              *s++ = '\t';
              rem = 0;
            }
        }

      while (rem--)
        *s++ = ' ';
    }
  *s = '\0';
}

int
main (int   argc,
      char *argv[])
{
  int n;
  int start = 0;
  int tabsize = 8;
  char s[MAX];

  while (--argc && (**++argv == '-' || **argv == '+'))
    {
      if ((n = atoi (*argv)) < 0)
        start = abs (n);
      else
        tabsize = n;
    }

  if (argc != 1)
    {
      printf ("Usage: -[START] +[TABSIZE] 'STRING'\n");
      return 1;
    }

  entab (s, *argv, start, tabsize);
  printf ("%s\n", s);

  return 0;
}
