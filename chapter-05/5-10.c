/*
 * Write the program expr, which evaluates a reverse Polish expression from the
 * command line, where each operator or operand is a separate argument. For
 * example,
 *
 *  expr 2 3 4 + *
 *
 * evaluates 2 × (3 + 4).
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX 256
#define NUM '0'

int sp;
double stack[MAX];

double
pop ()
{
  if (sp > 0)
    {
      return stack[--sp];
    }
  else
    {
      printf ("error: stack empty\n");
      return 0.0;
    }
}

void
push (double f)
{
  if (sp < MAX)
    stack[sp++] = f;
  else
    printf ("error: stack full, can't push\n");
}

int
getop (char *arg)
{
  while (isdigit (*arg))
    {
      if (!*++arg)
        return NUM;
    }

  return *arg;
}

int
main (int   argc,
      char *argv[])
{
  int type;
  double op2;

  while (--argc)
    {
      switch ((type = getop (*++argv)))
        {
        case NUM:
          push (atof (*argv));
          break;
        case '+':
          push (pop () + pop ());
          break;
        case '-':
          op2 = pop ();
          push (pop () - op2);
          break;
        case '*':
          push (pop () * pop ());
          break;
        case '/':
          op2 = pop ();
          push (pop () / op2);
          break;
        default:
          printf ("Usage: EXPRESSION\n");
          return 1;
        }
    }
  printf ("%g\n", pop ());

  return 0;
}
