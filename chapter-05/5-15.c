/*
 * Add the option -f to fold upper and lower case together, so that case
 * distinctions are not made during sorting; for example, a and A compare equal.
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFSIZE 4096
#define MAXLEN 128
#define MAXLINES 256

enum
{
  FOLD = 1,
  NUMERIC = 2,
  REVERSE = 4
};

int bufp;
char *lines[MAXLINES];

void
swap (void *v[],
      int   i,
      int   j)
{
  void *tmp;

  tmp = v[i];
  v[i] = v[j];
  v[j] = tmp;
}

int
getnline (char *s,
          int   lim)
{
  int c;
  char *start = s;

  while ((c = getchar ()) != '\n' && --lim)
    {
      if (c == EOF)
        return EOF;
      *s++ = c;
    }
  *s = '\0';

  return (s - start);
}

void
writelines (char *lines[],
            int   nlines)
{
  while (nlines--)
    printf ("%s\n", *lines++);
}

void
reverse (void *lines[],
         int   len)
{
  void *tmp;

  for (int i = 0; i < len; ++i, --len)
    {
      tmp = lines[i];
      lines[i] = lines[len];
      lines[len] = tmp;
    }
}

int
stricmp (const char *s1,
         const char *s2)
{
  while (*s1 && tolower (*s1) == tolower (*s2))
    {
      ++s1;
      ++s2;
    }

  return (!*s2) ? 0 : (tolower (*s1) - tolower (*s2));
}

int
numcmp (const char *s1,
        const char *s2)
{
  return (atof (s1) - atof (s2));
}

void
quicksort (void *v[],
           int   left,
           int   right,
           int   (*cmp) (void *, void *))
{
  int pivot;
  int last;

  if (left >= right)
    return;

  pivot = (left + right) / 2;
  swap (v, left, pivot);
  last = left;

  for (int i = left + 1; i <= right; ++i)
    {
      if ((*cmp) (v[i], v[left]) < 0)
        swap (v, ++last, i);
    }

  swap (v, left, last);
  quicksort (v, left, last - 1, cmp);
  quicksort (v, last + 1, right, cmp);
}

int
readlines (char *buf,
           char *lines[],
           int   lim)
{
  int len;
  char s[MAXLEN];
  int nlines = 0;

  while ((len = getnline (s, MAXLEN)) != EOF && len > 0)
    {
      if (nlines >= MAXLINES || bufp + len >= BUFSIZE)
        {
          return -1;
        }
      else
        {
          strcpy (buf, s);
          lines[nlines++] = buf;
          buf += len + 1;
          bufp += len;
        }
    }

  return nlines;
}

int
main (int   argc,
      char *argv[])
{
  char flags = 0;
  char buf[BUFSIZE];
  int nlines;

  while (--argc && **++argv == '-')
    {
      switch ((*argv)[1])
        {
        case 'f':
          flags |= FOLD;
          break;
        case 'n':
          flags |= NUMERIC;
          break;
        case 'r':
          flags |= REVERSE;
          break;
        default:
          printf ("Invalid option '%s'\n", *argv);
          return 1;
        }
    }

  if ((nlines = readlines (buf, lines, MAXLINES)) > 0)
    {
      if (flags & FOLD)
        quicksort ((void **) lines, 0, nlines - 1, (int (*) (void *, void *))
                  (flags & NUMERIC ? numcmp : stricmp));
      else
        quicksort ((void **)lines, 0, nlines - 1, (int (*) (void *, void *))
                  (flags & NUMERIC ? numcmp : strcmp));
      if (flags & REVERSE)
        reverse ((void **) lines, nlines - 1);

      writelines (lines, nlines);
    }

  return 0;
}
