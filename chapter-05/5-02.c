/*
 * Write getfloat, the floating-point analog of getint. What type does getfloat
 * return as its function value?
 */

#include <ctype.h>
#include <stdio.h>

#define MAX 256

char buf[MAX];
int bufp;

void
ungetch (int c)
{
  if (bufp >= MAX)
    printf ("error: too many characters\n");
  else
    buf[bufp++] = c;
}

int
getch ()
{
  return (bufp > 0) ? buf[--bufp] : getchar ();
}

int
getfloat (double *pf)
{
  int c;
  int sign;
  double val;
  double power;

  while (isspace (c = getch ()))
    ;

  if (!isdigit (c) && c != EOF && c != '+' && c != '-' && c != '.')
    {
      ungetch (c);
      return 0;
    }

  sign = (c == '-') ? -1 : 1;

  if (c == '+' || c == '-')
    c = getch ();
  if (!isdigit (c) && c != '.')
    {
      ungetch (c);
      return 0;
    }

  for (val = 0.0; isdigit (c); c = getch ())
    val = 10.0 * val + (c - '0');
  if (c == '.')
    c = getch ();
  for (power = 1.0; isdigit (c); c = getch ())
    {
      val = 10.0 * val + (c - '0');
      power *= 10.0;
    }
  *pf = sign * val / power;

  if (c != EOF)
    ungetch (c);

  return c;
}

int
main (int   argc,
      char *arv[])
{
  double f;

  if (getfloat (&f) > 0)
    printf ("%g\n", f);

  return 0;
}
