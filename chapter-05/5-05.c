/*
 * Write versions of the library functions strncpy, strncat, and strncmp, which
 * operate on at most the first n characters of their argument strings. For
 * example, strncpy(s,t,n) copies at most n characters of t to s. Full
 * descriptions are in Appendix B.
 */

#include <stdio.h>
#include <string.h>

#define MAX 256

void
_strncpy (char  *s,
          char  *t,
          size_t n)
{
  while ((*s++ = *t++) && --n)
    ;
  if (*s)
    *s = '\0';
}

int
_strncmp (char  *s,
          char  *t,
          size_t n)
{
  while ((*s++ == *t++) && n--)
    {
      if (!*s)
        return 0;
    }

  return (*--s - *--t);
}

void
_strncat (char  *s,
          char  *t,
          size_t n)
{
  s += strlen (s);

  while ((*s++ = *t++) && --n)
    ;
  if (*s)
    *s = '\0';
}

int
main (int   argc,
      char *argv[])
{
  char s[MAX] = "Kernighan";

  _strncat (s, " & Ritchie", 16);
  printf ("strncat: %s\n", s);

  printf ("strncmp: %d\n", _strncmp (s, "Ritchie", 4));

  _strncpy (s, "Ritchie", 4);
  printf ("strncpy: %s\n", s);

  return 0;
}
