/*
 * Rewrite readlines to store lines in an array supplied by main, rather than
 * calling alloc to maintain storage. How much faster is the program?
 */

#include <stdio.h>
#include <string.h>

#define BUFSIZE 4096
#define MAXLEN 128
#define MAXLINES 256

int bufp;
char *lines[MAXLINES];

int
getnline (char *s,
          int   lim)
{
  char *start = s;

  while ((*s++ = getchar ()) != '\n' && lim--)
    ;
  *--s = '\0';

  return (s - start);
}

void
writelines (char *lines[],
            int   nlines)
{
  while (nlines--)
    printf ("%s\n", *lines++);
}

int
readlines (char *buf,
           char *lines[],
           int   lim)
{
  int len;
  char s[MAXLEN];
  int nlines = 0;

  while ((len = getnline (s, MAXLEN)) > 0)
    {
      if (nlines >= MAXLINES || bufp >= BUFSIZE)
        {
          return -1;
        }
      else
        {
          strcpy (buf, s);
          lines[nlines++] = buf;
          buf += len + 1;
          bufp += len;
        }
    }

  return nlines;
}

int
main (int   argc,
      char *argv[])
{
  char buf[BUFSIZE];
  int nlines;

  if ((nlines = readlines (buf, lines, MAXLINES)) > 0)
    writelines (lines, nlines);

  return 0;
}
