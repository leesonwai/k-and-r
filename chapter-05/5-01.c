/*
 * As written, getint treats a + or + not followed by a digit as a valid
 * representation of zero. Fix it to push such a character back on the input.
 */

#include <ctype.h>
#include <stdio.h>

#define MAX 256

char buf[MAX];
int bufp;

void
ungetch (int c)
{
  if (bufp >= MAX)
    printf ("error: too many characters\n");
  else
    buf[bufp++] = c;
}

int
getch ()
{
  return (bufp > 0) ? buf[--bufp] : getchar ();
}

int
getint (int *pn)
{
  int c;
  int sign;

  while (isspace (c = getch ()))
    ;

  if (!isdigit (c) && c != EOF && c != '+' && c != '-')
    {
      ungetch (c);
      return 0;
    }

  sign = (c == '-') ? -1 : 1;

  if (c == '+' || c == '-')
    c = getch ();
  if (!isdigit (c))
    {
      ungetch (c);
      return 0;
    }

  for (*pn = 0; isdigit (c); c = getch ())
    *pn = 10 * *pn + (c - '0');
  *pn *= sign;

  if (c != EOF)
    ungetch (c);

  return c;
}

int
main (int   argc,
      char *argv[])
{
  int n;

  if (getint (&n) > 0)
    printf ("%d\n", n);

  return 0;
}
