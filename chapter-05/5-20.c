/*
 * Expand dcl to handle declarations with function argument types, qualifiers
 * like const, and so on.
 */

#include <ctype.h>
#include <stdio.h>
#include <string.h>

#define BUFSIZE 32
#define MAXOUT 1024
#define MAXTOKEN 128
#define MAXTYPE 256

enum {
  BRACKETS,
  NAME,
  PARENS
};

int dcl (void);
int gettoken (void);

char buf[BUFSIZE];
int bufp;
char datatype[MAXTYPE];
char name[MAXTOKEN];
char out[MAXOUT];
char token[MAXTOKEN];
int tokentype;

void
ungetch (int c)
{
  if (bufp >= BUFSIZE)
    printf ("error: too many characters\n");
  else
    buf[bufp++] = c;
}

int
getch ()
{
  return (bufp > 0) ? buf[--bufp] : getchar ();
}

int
dirdcl ()
{
  int type;

  if (tokentype == '(')
    {
      dcl ();
      if (tokentype != ')')
        {
          printf ("error: missing ')'\n");
          return 0;
        }
    }
  else if (tokentype == NAME)
    {
      strcpy (name, token);
    }
  else
    {
      printf ("error: expected name or dcl\n");
      return 0;
    }

  while ((type = gettoken ()) == BRACKETS || type == PARENS)
    {
      if (type == BRACKETS)
        {
          strcat (out, " array");
          strcat (out, token);
          strcat (out, " of");
        }
      else if (type == PARENS)
        {
          strcat (out, " function expecting ");
          strcat (out, token);
          strcat (out, " returning");
        }
    }

  return 1;
}

int
dcl ()
{
  int ns;

  for (ns = 0; gettoken () == '*'; )
    ++ns;
  if (!dirdcl ())
    return 0;
  while (ns--)
    strcat (out, " pointer to");

  return 1;
}

int
gettoken ()
{
  int c;
  char *p = token;
  int tp = token - p;

  while ((c = getch ()) == ' ' || c == '\t')
    ;

  if (c == '(')
    {
      while ((isalnum (c = getch ()) || c == ',') && c != ')')
        *p++ = c;
      if (token - p == tp)
        strcpy (p, "void");
      else
        *p = '\0';
      return tokentype = PARENS;
    }
  else if (c == '[')
    {
      for (*p++ = c; (*p++ = getch ()) != ']'; )
        ;
      *p = '\0';
      return tokentype = BRACKETS;
    }
  else if (isalpha (c))
    {
      for (*p++ = c; isalnum (c = getch ()); )
        *p++ = c;
      *p = '\0';
      ungetch (c);
      return tokentype = NAME;
    }
  else
    {
      return tokentype = c;
    }
}

int
main (int   argc,
      char *argv[])
{
  while (gettoken () != EOF)
    {
      out[0] = '\0';
      if (strcmp (token, "static") == 0 && gettoken () == NAME)
        strcpy (out, "static");
      if (strcmp (token, "const") == 0 && gettoken () == NAME)
        sprintf (datatype, "%s %s", token, "constant");
      else
        strcpy (datatype, token);
      if (dcl ())
        printf ("%s: %s %s\n", name, out, datatype);
    }

  return 0;
}
