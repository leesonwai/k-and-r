/*
 * Write a function expand(s1,s2) that expands shorthand notations like a-z in
 * the string s1 into the quivalent complete list abc...xyz in s2. Allow for
 * letters of either case and digits, and be prepared to handle cases like
 * a-b-c and a-z0-9 and -a-z. Arrange that a leading or trailing - is taken
 * literally.
 */

#include <ctype.h>
#include <stdio.h>
#include <string.h>

#define MAX 256

void
expand (char s1[],
        char s2[])
{
  int i;
  int j;
  int start;
  int end;

  for (i = 0, j = 0; i < strlen (s1); ++i)
    {
      if (s1[i] == '-' && isalnum (s1[i - 1]) && isalnum (s1[i + 1]))
        {
          --j; /* Ensure duplicate start char is overwritten */
          start = s1[i - 1];
          while (s1[i + 2] == '-')
            i += 2;
          end = s1[++i];
          while (start <= end)
            s2[j++] = start++;
        }
      else
        {
          s2[j++] = s1[i];
        }
    }
  s2[j] = '\0';
}

int
main (int   argc,
      char *argv[])
{
  char s1[] = "Hello, test case: -a-f-u0-5-9!-";
  char s2[MAX];

  expand (s1, s2);
  printf ("%s\n", s2);

  return 0;
}
