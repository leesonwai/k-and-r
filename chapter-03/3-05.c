/*
 * Write the function itob(n,s,b) that converts the integer n into a base b
 * character representation in the string s. In particular, itob(n,s,16)
 * formats n as a hexadecimal integer in s.
 */

#include <stdio.h>
#include <string.h>

#define MAX 128

void
reverse (char s[])
{
  for (int i = 0, j = strlen (s) - 1; i < j; ++i, --j)
    {
      int tmp = s[i];
      s[i] = s[j];
      s[j] = tmp;
    }
}

void
itob (int  n,
      char s[],
      int  b)
{
  int i = 0;

  do
    s[i++] = (n % b < 10) ? n % b + '0' : n % b + ('A' - 10);
  while (n /= b);
  s[i] = '\0';
  reverse (s);
}

int
main (int   argc,
      char *argv[])
{
  char s[MAX];

  itob (966, s, 16);
  printf ("%s\n", s);

  return 0;
}
