/*
 * Write a function escape(s,t) that converts characters like newline and tab
 * into visible escape sequences like \n and \t as it copies the string t to s.
 * Use a switch. Write a function for the other direction as well, converting
 * escape sequences into the real characters.
 */

#include <stdio.h>
#include <string.h>

#define MAX 256

void
trap (char s[],
      char t[])
{
  int i;
  int j;

  for (i = j = 0; i < strlen (s); ++i, ++j)
    {
      if (s[i] == '\\')
        {
          switch (s[i + 1])
            {
            case '"':
              t[j] = '\"';
              ++i;
              break;
            case '\\':
              t[j] = '\\';
              ++i;
              break;
            case 'b':
              t[j] = '\b';
              ++i;
              break;
            case 'n':
              t[j] = '\n';
              ++i;
              break;
            case 't':
              t[j] = '\t';
              ++i;
              break;
            default:
              t[j] = s[i];
              break;
            }
        }
      else
        {
          t[j] = s[i];
        }
    }
  t[j] = '\0';
}

void
escape (char s[],
        char t[])
{
  int i;
  int j;

  for (i = j = 0; i < strlen (s); ++i, ++j)
    {
      switch (s[i])
        {
        case '\"':
          t[j++] = '\\';
          t[j] = '"';
          break;
        case '\\':
          t[j++] = '\\';
          t[j] = '\\';
          break;
        case '\b':
          t[j++] = '\\';
          t[j] = 'b';
          break;
        case '\n':
          t[j++] = '\\';
          t[j] = 'n';
          break;
        case '\t':
          t[j++] = '\\';
          t[j] = 't';
          break;
        default:
          t[j] = s[i];
          break;
        }
    }
  t[j] = '\0';
}

int
main (int   argc,
      char *argv[])
{
  char s[] = "Hello,\tescape sequencess\b!";
  char t[MAX];

  escape (s, t);
  printf ("%s\n", t);
  trap (s, t);
  printf ("%s\n", t);

  return 0;
}
