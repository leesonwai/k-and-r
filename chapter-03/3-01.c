/*
 * Our binary search makes two tests inside the loop, when one would suffice
 * (at the price of more tests outside). Write a version with only one test
 * inside the loop and measure the difference in run-time.
 */

#include <stdio.h>

#define MAX 4096

int
binsearch (int x,
           int v[],
           int n)
{
  int low = 0;
  int high = n - 1;
  int mid;

  while (low < high)
    {
      mid = (low + high + 1) / 2;

      if (x < v[mid])
        high = mid - 1;
      else
        low = mid;
    }

  return (v[low] == x) ? low : -1;
}

int
main (int   argc,
      char *argv[])
{
  int list[MAX];

  for (int i = 0; i < MAX; ++i)
    list[i] = i;
  printf ("%d\n", binsearch (2048, list, MAX));

  return 0;
}
