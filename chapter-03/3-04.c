/*
 * In a two's complement number representation, our version of itoa does not
 * handle the largest negative number, that is, the value of n equal to
 * -(2^wordsize-1). Explain why not. Modify it to print that value correctly,
 * regardless of the machine on which it runs.
 */

#include <limits.h>
#include <stdio.h>
#include <string.h>

#define MAX 10

void
reverse (char s[])
{
  for (int i = 0, j = strlen (s) - 1; i < j; ++i, --j)
    {
      int tmp = s[i];
      s[i] = s[j];
      s[j] = tmp;
    }
}

void
itoa (int  n,
      char s[])
{
  int i = 0;
  int sign = (n < 0) ? -1 : 1;

  do
    s[i++] = sign * (n % 10) + '0';
  while (n /= 10);
  if (sign == -1)
    s[i++] = '-';
  s[i] = '\0';
  reverse (s);
}

int
main (int   argc,
      char *argv[])
{
  char s[MAX];

  itoa (INT_MIN, s);
  printf ("%s\n", s);

  return 0;
}
