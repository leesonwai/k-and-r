/*
 * Write a version of itoa that accepts three arguments instead of two. The
 * third argument is a minimum field width; the converted number must be padded
 * with blanks on the left if necessary to make it wide enough.
 */

#include <stdio.h>
#include <string.h>

#define MAX 128

void
reverse (char s[])
{
  for (int i = 0, j = strlen (s) - 1; i < j; ++i, --j)
    {
      int tmp = s[i];
      s[i] = s[j];
      s[j] = tmp;
    }
}

void
itoa (int  n,
      char s[],
      int  w)
{
  int i = 0;
  int sign = (n < 0) ? -1 : 1;

  do
    s[i++] = sign * (n % 10) + '0';
  while (n /= 10);
  if (sign == -1)
    s[i++] = '-';
  while (i < w)
    s[i++] = ' ';
  reverse (s);
}

int
main (int   argc,
      char *argv[])
{
  char s[MAX];

  itoa (2048, s, 16);
  printf ("%s\n", s);

  return 0;
}
