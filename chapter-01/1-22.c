/*
 * Write a program to "fold" long input lines into two or more shorter lines
 * after the last non-blank character that occurs before the n-th column of
 * input. Make sure your program does something intelligent with very long
 * lines, and if there are no blanks or tabs before the specified column.
 */
 
#include <stdio.h>
 
#define LIM 80
#define MAX 256

void
fold (char line[],
      int  i)
{
  while (line[i] != ' ')
    --i;
  line[i] = '\n';
}

int
main (int   argc,
      char *argv[])
{
  char line[MAX];
  int i = 0;

  while (i < MAX - 1 && (line[i] = getchar ()) != EOF)
    {
      if (i > 0 && i % (LIM - 1) == 0)
        fold (line, i);
      ++i;
    }
  line[i] = '\0';
  printf ("%s\n", line);

  return 0;
}
