/*
 * Write a program to remove all comments from a C program. Don't forget to
 * handle quoted strings and character constants properly. C comments do not
 * nest.
 */

#include <stdio.h>

void
eat (int next_c)
{
  int c;

  if (next_c != '*')
    {
      putchar ('/');
      putchar (next_c);
      return;
    }

  while ((c = getchar ()) != '/')
    ; /* No-op to eat comment */
}

int
main (int   argc,
      char *argv[])
{
  int c;

  while ((c = getchar ()) != EOF)
    {
      if (c == '/')
        eat (getchar ());
      else
        putchar (c);
    }

  return 0;
}
