/* 
 * Write a program to copy its input to its output, replacing each string of
 * one or more blanks by a single blank.
 */

#include <stdio.h>

int
main (int   argc,
      char *argv[])
{
  int c;
  int b = 0;

  while ((c = getchar ()) != EOF)
    {
      if (c == ' ')
        {
          ++b;
        }
      else if (c != ' ' && b > 0)
        {
          putchar (' ');
          putchar (c);
          b = 0;
        }
      else
        {
          putchar (c);
        }
    }

  return 0;
}
