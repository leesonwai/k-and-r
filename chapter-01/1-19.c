/* 
 * Write a function reverse(s) that reverses that character string s. Use it to
 * write a program that reverses its input a line at a time.
 */

#include <stdio.h>

#define MAX 256

void
reverse (char s[])
{
  int len = 0;

  while (s[len] != '\0')
    ++len;
  for (int i = 0, j = len - 1; i < j; ++i, --j)
    {
      int tmp = s[i];
      s[i] = s[j];
      s[j] = tmp;
    }
}

int
main (int   argc,
      char *argv[])
{
  int i = 0;
  char s[MAX];

  while (i < MAX - 1 && (s[i] = getchar ()) != EOF)
    {
      if (s[i] == '\n')
        {
          s[i] = '\0';
          reverse (s);
          printf ("%s\n", s);
          i = 0;
        }
      else
        {
          ++i;
        }
    }

  return 0;
}
