/*
 * Write a program to remove trailing blanks and tabs from each line of input,
 * and to delete entirely blank lines.
 */

#include <stdio.h>

#define MAX 256

void
trim (char line[])
{
  int len = 0;

  while (line[len] != '\n')
    ++len;
  while (line[len] == '\n' || line[len] == ' ' || line[len] == '\t')
    --len;
  line[len + 1] = '\0';
}

int
main (int   argc,
      char *argv[])
{
  int i = 0;
  char line[MAX];

  while (i < MAX - 1 && (line[i] = getchar ()) != EOF)
    {
      if (line[i] == '\n')
        {
          trim (line);
          if (line[0] != '\0')
            printf ("%s\n", line);
          i = 0;
        }
      else
        {
          ++i;
        }
    }

  return 0;
}
