/* 
 * Write a program to print a histogram of the lengths of words in its input.
 * It is easy to draaw the histogram with the bars horizontal; a vertical
 * orientation is more challenging.
 */

#include <stdio.h>

#define MAX 10

int
main (int   argc,
      char *argv[])
{
  int c;
  int hist[MAX] = {0};
  int len = 0;

  while ((c = getchar ()) != EOF)
    {
      if (c == ' ' || c == '\n')
        {
          ++hist[len];
          len = 0;
        }
      ++len;
    }

  for (int i = 0; i < MAX; ++i)
    {
      printf ("%d", i);
      for (int j = 0; j < hist[i]; ++j)
        printf ("|");
      printf ("\n");
    }

  return 0;
}
