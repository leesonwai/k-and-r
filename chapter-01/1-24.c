/*
 * Write a program to check a C program for rudimentary syntax errors like
 * unbalanced parentheses, brackets and braces. Don't forget about quotes, both
 * single and double, escape sequences, and comments.
 */
 
#include <stdio.h>

int
flip (int bracket)
{
  if (bracket == '(')
    return ')';
  else if (bracket == '[')
    return ']';
  else
    return '}';
}

void
parse (int bracket,
       int pos[])
{
  int c;

  while ((c = getchar ()) != EOF)
    {
      if (c == '(' || c == '[' || c == '{')
        parse (c, pos);

      if (c == flip (bracket))
        {
          return;
        }
      else if (c == ';')
        {
          printf ("%d:%d: error: missing '%c'\n",
                  pos[0], pos[1], flip (bracket));
          return;
        }

      if (c == '\n')
        {
          ++pos[0];
          pos[1] = 1;
        }
      else
        {
          ++pos[1];
        }
    }
}

int
main (int   argc,
      char *argv[])
{
  int c;
  int pos[2] = {1, 1}; /* 0: line, 1: column */

  while ((c = getchar ()) != EOF)
    {
      if (c == '(' || c == '[' || c == '{')
        parse (c, pos);

      if (c == '\n')
        {
          ++pos[0];
          pos[1] = 1;
        }
      else
        {
          ++pos[1];
        }
    }

  return 0;
}
