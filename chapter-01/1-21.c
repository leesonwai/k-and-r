/* 
 * Write a program entab that replaces strings of blanks by the minimum number
 * of tabs and blanks to achieve the same spacing. Use the same tab stops as
 * for detab. When either a tab or a single blank would suffice to reach a tab
 * stop, which should be given preference?
 */

#include <stdio.h>

#define TAB 8

int col = 1;

void
entab (int next_c)
{
  int c;
  int end;
  int rem;

  ++col; /* Account for next_c */

  if (next_c != ' ')
    {
      putchar (' ');
      putchar (next_c);
      return;
    }

  end = col;
  while ((c = getchar()) == ' ')
    ++end;
  rem = end - col;

  while (col < end)
    {
      if (col % TAB == 0)
        {
          putchar ('\t');
          rem = col;
        }
      ++col;
    }

  while (rem < end)
    {
      putchar (' ');
      ++rem;
    }
  putchar (c);
  ++col; /* Account for c */
}

int
main (int   argc,
      char *argv[])
{
  int c;

  while ((c = getchar ()) != '\n')
    {
      if (c == ' ')
        entab (getchar ());
      else
        putchar (c);
      ++col;
    }
  putchar ('\n');

  return 0;
}
