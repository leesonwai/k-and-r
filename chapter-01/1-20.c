/* 
 * Write a program detab that replaces tabs in the input with the proper number
 * of blanks to space to the next tab stop. Assume a fixed set of tabs stops,
 * say every n columns. Should n be a variable or a symbolic parameter?
 */

#include <stdio.h>

#define TAB 8

int
main (int   argc,
      char *argv[])
{
  int c;
  int col = 1;

  while ((c = getchar ()) != EOF)
    {
      if (c == '\t')
        {
          for (int i = 0; i < TAB - (col % TAB); ++i)
            putchar (' ');
          col += TAB - (col % TAB);
        }
      else if (c == '\n')
        {
          putchar (c);
          col = 1;
        }
      else
        {
          putchar (c);
          ++col;
        }
    }
}
