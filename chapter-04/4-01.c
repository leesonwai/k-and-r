/*
 * Write the function strrindex(s,t), which returns the position of the
 * rightmost occurence of t in s, or -1 if there is none.
 */

#include <stdio.h>
#include <string.h>

int
strrindex (char s[],
           char t[])
{
  int i;
  int j;
  int k;

  for (i = strlen (s); i > 0; --i)
    {
      for (j = i, k = 0; t[k] != '\0' && s[j] == t[k]; ++j, ++k)
        ; /* No-op */
      if (k > 0 && t[k] == '\0')
        return i;
    }

  return -1;
}

int
main (int   argc,
      char *argv[])
{
  char s[] = "Hello, world, hello!";
  char t[] = "ello";

  printf ("%d\n", strrindex (s, t));

  return 0;
}
