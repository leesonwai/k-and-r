/*
 * Add commands to print the top element of the stack without popping, to
 * duplicate it, and to swap the top two elements. Add a command to clear the
 * stack.
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

#define ARG '!'
#define BUFSIZE 128
#define MAXOP 128
#define MAXVAL 128
#define NUM '0'

char buf[BUFSIZE];
int bufp;
int sp;
double stack[MAXVAL];

void
ungetch (int c)
{
  if (bufp >= BUFSIZE)
    printf ("error: too many characters\n");
  else
    buf[bufp++] = c;
}

int
getch ()
{
  return (bufp > 0) ? buf[--bufp] : getchar ();
}

double
pop ()
{
  if (sp > 0)
    {
      return stack[--sp];
    }
  else
    {
      printf ("error: stack empty\n");
      return 0.0;
    }
}

void
push (double f)
{
  if (sp < MAXVAL)
    stack[sp++] = f;
  else
    printf ("error: stack full, can't push\n");
}

int
getop (char s[])
{
  int c;
  int i = 0;

  while ((s[0] = c = getch ()) == ' ' || c == '\t')
    ;
  s[1] = '\0';

  if (c == '!')
    {
      s[++i] = getch ();
      return ARG;
    }

  if (!isdigit (c) && c != '.')
    return c;
  if (isdigit (c))
    {
      while (isdigit (s[++i] = c = getch ()))
        ;
    }
  if (c == '.')
    {
      while (isdigit (s[++i] = c = getch ()))
        ;
    }
  s[i] = '\0';

  if (c != EOF)
    ungetch (c);

  return NUM;
}

int
main (int   argc,
      char *argv[])
{
  int type;
  char s[MAXOP];
  double op1;
  double op2;

  while ((type = getop (s)) != EOF)
    {
      switch (type)
        {
        case ARG:
          switch (s[1])
            {
            case 'c':
              sp = 0;
              printf ("stack cleared\n");
              break;
            case 'd':
              push (stack[sp - 1]);
              printf ("top duplicated\n");
              break;
            case 'p':
              printf ("top: %.8g\n", stack[sp - 1]);
              break;
            case 's':
              op1 = pop ();
              op2 = pop ();
              push (op1);
              push (op2);
              printf ("top swapped\n");
              break;
            default:
              printf ("error: command not recognised\n");
              break;
            }
          break;
        case NUM:
          push (atof (s));
          break;
        case '+':
          push (pop () + pop ());
          break;
        case '-':
          op2 = pop ();
          push (pop () - op2);
          break;
        case '*':
          push (pop () * pop ());
          break;
        case '/':
          op2 = pop ();
          if (op2 == 0.0)
            printf ("error: zero divisor\n");
          else
            push (pop () / op2);
          break;
        case '\n':
          printf ("%.8g\n", pop ());
          break;
        default:
          printf ("error: unknown command\n");
          break;
        }
    }

  return 0;
}
