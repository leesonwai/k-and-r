/*
 * Write a recursive version of the function reverse(s), which reverses the
 * string s in place.
 */

#include <stdio.h>
#include <string.h>

void
reverse (int  start,
         int  end,
         char s[])
{
  if (start < end)
    {
      int tmp = s[start];
      s[start++] = s[end];
      s[end--] = tmp;
      reverse (start, end, s);
    }
}

int
main (int   argc,
      char *argv[])
{
  char s[] = "Hello, world!";

  reverse (0, (strlen (s) - 1), s);
  printf ("%s\n", s);

  return 0;
}
