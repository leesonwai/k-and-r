/*
 * Define a macro swap(t,x,y) that interchanges two arguments of type t. (Block
 * structures will help.)
 */

#include <stdio.h>

#define SWAP(t, x, y) t tmp; tmp = x; x = y; y = tmp

int
main (int   argc,
      char *argv[])
{
  int x = 42;
  int y = 24;

  SWAP (int, x, y);
  printf ("%d %d\n", x, y);

  return 0;
}
