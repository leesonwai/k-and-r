/*
 * Add commands for handling variables. (It's easy to provide twenty-six
 * variables with single-letter names.) Add a variable for the most recently
 * printed value.
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

#define ALPHABET 26
#define BUFSIZE 128
#define MAXOP 128
#define MAXVAL 128
#define NUM '0'
#define VAR 'a'

char buf[BUFSIZE];
int bufp;
int sp;
double stack[MAXVAL];
int var;
double vars[ALPHABET];

void
ungetch (int c)
{
  if (bufp >= BUFSIZE)
    printf ("error: too many characters\n");
  else
    buf[bufp++] = c;
}

int
getch ()
{
  return (bufp > 0) ? buf[--bufp] : getchar ();
}

double
pop ()
{
  if (sp > 0)
    {
      return stack[--sp];
    }
  else
    {
      printf ("error: stack empty\n");
      return 0.0;
    }
}

void
push (double f)
{
  if (sp < MAXVAL)
    stack[sp++] = f;
  else
    printf ("error: stack full, can't push\n");
}

int
getop (char s[])
{
  int c;
  int i = 0;

  while ((s[0] = c = getch ()) == ' ' || c == '\t')
    ;
  s[1] = '\0';

  if (islower (c))
    {
      s[i] = c;
      return VAR;
    }

  if (!isdigit (c) && c != '.')
    return c;
  if (isdigit (c))
    {
      while (isdigit (s[++i] = c = getch ()))
        ;
    }
  if (c == '.')
    {
      while (isdigit (s[++i] = c = getch ()))
        ;
    }
  s[i] = '\0';

  if (c != EOF)
    ungetch (c);

  return NUM;
}

int
main (int   argc,
      char *argv[])
{
  int type;
  char s[MAXOP];
  double op2;

  for (int i = 0; i < ALPHABET; ++i)
    vars[i] = -1;

  while ((type = getop (s)) != EOF)
    {
      switch (type)
        {
        case NUM:
          push (atof (s));
          break;
        case VAR:
          var = s[0] - 'a';
          if (vars[var] == -1)
            ; /* No-op */
          else
            push (vars[var]);
          break;
        case '+':
          push (pop () + pop ());
          break;
        case '-':
          op2 = pop ();
          push (pop () - op2);
          break;
        case '*':
          push (pop () * pop ());
          break;
        case '/':
          op2 = pop ();
          if (op2 == 0.0)
            printf ("error: zero divisor\n");
          else
            push (pop () / op2);
          break;
        case '=':
          vars[var] = pop ();
        case '\n':
          vars['z' - 'a'] = pop ();
          printf ("%.8g\n", vars['z' - 'a']);
          break;
        default:
          printf ("error: unknown command\n");
          break;
        }
    }

  return 0;
}
