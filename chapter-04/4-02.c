/*
 * Extend atof to handle scientific notation of the form 123.45e-6 where a
 * floating-point number may be followed by e or E and an optionally signed
 * exp.
 */

#include <ctype.h>
#include <stdio.h>
#include <math.h>

double
atof (char s[])
{
  int i;
  int sign;
  double val;
  double power;
  double exp;

  for (i = 0; isspace (s[i]); ++i)
    ;
  sign = (s[i] == '-') ? -1 : 1;
  if (s[i] == '+' || s[i] == '-')
    ++i;
  for (val = 0.0; isdigit (s[i]); ++i)
    val = 10.0 * val + (s[i] - '0');
  if (s[i] == '.')
    ++i;
  for (power = 1.0; isdigit (s[i]); ++i)
    {
      val = 10.0 * val + (s[i] - '0');
      power *= 10.0;
    }
  val = sign * val / power;
  if (s[i] == 'e' || s[i] == 'E')
    ++i;
  sign = (s[i] == '-') ? -1 : 1;
  if (s[i] == '+' || s[i] == '-')
    ++i;
  for (exp = 0; isdigit (s[i]); ++i)
    exp = 10 * exp + (s[i] - '0');
  exp *= sign;

  return (exp > 0) ? pow (val, exp) : val;
}

int
main (int   argc,
      char *argv[])
{
  printf ("%g\n", atof ("2.0e7"));
}
