/*
 * An alternate organization uses getline to read an entire input line; this
 * makes getch and ungetch unnecessary. Revise the calculator to use this
 * approach.
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX 256

int sp;
int stack[MAX];

double
pop ()
{
  if (sp > 0)
    {
      return stack[--sp];
    }
  else
    {
      printf ("error: stack empty\n");
      return 0;
    }
}

void
push (double f)
{
  if (sp < MAX - 1)
    stack[sp++] = f;
  else
    printf ("error: stack is full\n");
}

void
eval (char expr[])
{
  int i = 0;
  char op[MAX];

  while (expr[i] != '\0')
    {
      if (isdigit (expr[i]))
        {
          int j = 0;

          while (isdigit (expr[i]))
            op[j++] = expr[i++];
          op[j] = '\0';
          push (atof (op));
        }
      if (expr[i] == '+')
        push (pop () + pop ());
      if (!isdigit (expr[i]) && !isspace (expr[i]))
        printf ("error: unknown command\n");
      ++i;
    }
  printf ("%.8g\n", pop ());
}

int
getexpr (char expr[])
{
  int c;
  int len = 0;

  while ((c = getchar ()) != '\n' && len < MAX - 1)
    expr[len++] = c;
  expr[len] = '\0';

  return len;
}

int
main (int   argc,
      char *argv[])
{
  char expr[MAX];

  while ((getexpr (expr)) > 0)
    eval (expr);

  return 0;
}
