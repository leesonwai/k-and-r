/*
 * Write a routine ungets(s) that will push back an entire string onto the
 * input. Should ungets know about buf andd bufp, or should it just use
 * ungetch()?
 */

#include <stdio.h>
#include <string.h>

#define MAX 256

char buf[MAX];
int bufp;

void
ungetch (int c)
{
  if (bufp >= MAX)
    printf ("error: too many characters\n");
  else
    buf[bufp++] = c;
}

int
getch ()
{
  return (bufp > 0) ? buf[--bufp] : getchar ();
}

void
ungets (char s[])
{
  int len = strlen (s) + 1; /* Ensure we get the null character */

  while (len)
    ungetch (s[--len]);
}

int
main ()
{
  char s[] = "Hello, world!";
  int c;

  ungets (s);
  while ((c = getch ()) != '\0')
    putchar (c);
  putchar ('\n');

  return 0;
}
