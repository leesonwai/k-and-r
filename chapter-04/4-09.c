/*
 * Our getch and ungetch do not handle a pushed-back EOF correctly. Decide what
 * their properties ought to be if an EOF is pushed back, then implement your
 * design.
 */

#include <stdio.h>

#define MAX 256

char buf[MAX];
int bufp;

int
getch ()
{
  return (bufp > 0) ? buf[--bufp] : getchar ();
}

void
ungetch (int c)
{
  if (bufp >= MAX)
    printf ("error: too many characters\n");
  else
    buf[bufp++] = c;
}

int
main (int   argc,
      char *argv[])
{
  int c;

  /* This works as expected: a pushed-back EOF terminates the loop */
  ungetch (EOF);
  while ((c = getch ()) != EOF)
    putchar (c);
  putchar ('\n');

  return 0;
}
