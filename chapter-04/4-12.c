/*
 * Adapt the ideas of printd to write a recursive version of itoa; that is,
 * convert an integer into a string by calling a recursive routine.
 */

#include <stdio.h>
#include <string.h>

#define MAX 128

void
itoa (int  n,
      char s[])
{
  static int i;

  if (n < 0)
    {
      s[i++] = '-';
      n = -n;
    }

  if (n / 10)
    itoa (n / 10, s);
  s[i++] = (n % 10) + '0';
  s[i] = '\0';
}

int
main (int   argc,
      char *argv[])
{
  char s[MAX];

  itoa (-11142, s);
  printf ("%s\n", s);

  return 0;
}
