/*
 * Modify getop so that it doesn't need to use ungetch. Hint: use an internal
 * static variable.
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX 256
#define NUM '0'

int buf[MAX];
int sp;
int stack[MAX];

double
pop ()
{
  return stack[--sp];
}

void
push (double f)
{
  stack[sp++] = f;
}

int
getop (char s[])
{
  int c;
  int i = 0;
  static int bufp;

  while ((s[0] = c = getchar ()) == ' ')
    ;
  s[1] = '\0';

  if (!isdigit (c))
    return c;
  if (isdigit (c))
    while (isdigit (s[++i] = c = getchar ()))
      ;
  s[i] = '\0';

  if (c != ' ' && c != '\n' && c != EOF)
    buf[bufp++] = c;

  return (bufp > 0) ? buf[--bufp] : NUM;
}

int
main (int   argc,
      char *argv[])
{
  int type;
  char s[MAX];

  while ((type = getop (s)) != EOF)
    {
      switch (type)
        {
        case NUM:
          push (atof (s));
          break;
        case '+':
          push (pop () + pop ());
          break;
        case '\n':
          printf ("%.8g\n", pop ());
          break;
        default:
          printf ("error: unknown command\n");
          break;
        }
    }

  return 0;
}
