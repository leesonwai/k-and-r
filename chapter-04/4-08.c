/*
 * Suppose that there will never be more than one character of pushback. Modify
 * getch and ungetch accordingly.
 */

#include <stdio.h>

#define CLEAN -1
#define MAX 256

int buf;

int
getch ()
{
  int c = buf;

  if (buf != CLEAN)
    {
      buf = CLEAN;
      return c;
    }

  return getchar ();
}

void
ungetch (int c)
{
  buf = c;
}

int
main (int   argc,
      char *argv[])
{
  int c;

  ungetch ('H');
  while ((c = getch ()) != EOF)
    putchar (c);

  return 0;
}
