/*
 * Add access to library functions like sin, exp and pow. See <math.h> in
 * Appendix B, Section 4.
 */

#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define BUFSIZE 128
#define FN '('
#define MAXOP 128
#define MAXVAL 128
#define NUM '0'

char buf[BUFSIZE];
int bufp;
int sp;
double stack[MAXVAL];

void
ungetch (int c)
{
  if (bufp >= BUFSIZE)
    printf ("error: too many characters\n");
  else
    buf[bufp++] = c;
}

int
getch ()
{
  return (bufp > 0) ? buf[--bufp] : getchar ();
}

double
pop ()
{
  if (sp > 0)
    {
      return stack[--sp];
    }
  else
    {
      printf ("error: stack empty\n");
      return 0.0;
    }
}

void
push (double f)
{
  if (sp < MAXVAL)
    stack[sp++] = f;
  else
    printf ("error: stack full, can't push\n");
}

int
getop (char s[])
{
  int c;
  int i = 0;

  while ((s[0] = c = getch ()) == ' ' || c == '\t')
    ;
  s[1] = '\0';

  if (c == '(')
    {
      s[i] = getch ();
      return FN;
    }

  if (!isdigit (c) && c != '.')
    return c;
  if (isdigit (c))
    {
      while (isdigit (s[++i] = c = getch ()))
        ;
    }
  if (c == '.')
    {
      while (isdigit (s[++i] = c = getch ()))
        ;
    }
  s[i] = '\0';

  if (c != EOF)
    ungetch (c);

  return NUM;
}

int
main (int   argc,
      char *argv[])
{
  int type;
  char s[MAXOP];
  double op2;

  while ((type = getop (s)) != EOF)
    {
      switch (type)
        {
        case FN:
          switch (s[1])
            {
            case 'e':
              push (exp (pop ()));
              break;
            case 'p':
              op2 = pop ();
              push (pow (pop (), op2));
              break;
            case 's':
              push (sin (pop ()));
              break;
            default:
              printf ("error: function not recognised\n");
              break;
            }
          break;
        case NUM:
          push (atof (s));
          break;
        case '+':
          push (pop () + pop ());
          break;
        case '-':
          op2 = pop ();
          push (pop () - op2);
          break;
        case '*':
          push (pop () * pop ());
          break;
        case '/':
          op2 = pop ();
          if (op2 == 0.0)
            printf ("error: zero divisor\n");
          else
            push (pop () / op2);
          break;
        case '\n':
          printf ("%.8g\n", pop ());
          break;
        default:
          printf ("error: unknown command\n");
          break;
        }
    }

  return 0;
}
