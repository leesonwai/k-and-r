/*
 * Write the function htoi(s), which converts a string of hexadecimal digits
 * (including an optional 0x or 0X) into its equivalent integer value. The
 * allowable digits are 0 through 9, a through f, and A through F.
 */

#include <stdio.h>

#define MAX 16

int
htoi (char h[])
{
  int id;
  int n = 0;

  if (h[1] == 'x' || h[1] == 'X')
    id = 2;
  else
    id = 0;
  for (int i = 0 + id; h[i] != '\0'; ++i)
    {
      if (h[i] >= '0' && h[i] <= '9')
        n = n * 16 + (h[i] - '0');
      else if (h[i] >= 'a' && h[i] <= 'z')
        n = n * 16 + (h[i] - 'a' + 10);
      else if (h[i] >= 'A' && h[i] <= 'Z')
        n = n * 16 + (h[i] - 'A' + 10);
    }

  return n;
}

int
main (int   argc,
      char *argv[])
{
  char h[] = "0x24AF";

  printf ("%s(16) = %d(10)\n", h, htoi (h));

  return 0;
}
