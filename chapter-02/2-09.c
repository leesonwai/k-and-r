/*
 * In a two's complement number system, x &= (x-1) deletes the rightmost 1-bit
 * in x. Explain why. Use this observation to write a faster version of
 * bitcount.
 */

#include <stdio.h>

int
bitcount (unsigned int x)
{
  int b = 0;

  while (x)
    {
      ++b;
      x &= x - 1;
    }

  return b;
}

int
main (int   argc,
      char *argv[])
{
  printf ("%d\n", bitcount (108));

  return 0;
}
