/*
 * Write a loop equivalent for the for loop above without using && or ||.
 */

#include <stdio.h>

#define MAX 256

void
get_line (char s[])
{
  int i;
  int c;

  for (i = 0; i < MAX; ++i)
    {
      if ((c = getchar()) == '\n')
        {
          s[i] = '\0';
          return;
        }
      else if (c == EOF)
        {
          s[i] = '\0';
          return;
        }
      else
        {
          s[i] = c;
        }
    }
  s[i] = '\0';
}

int
main (int   argc,
      char *argv[])
{
  char s[MAX];

  get_line (s);
  printf ("%s\n", s);

  return 0;
}
