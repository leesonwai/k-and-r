/*
 * Rewrite the function lower, which converts upper case letters to lower case,
 * with a conditional expression instead of if-else.
 */

#include <stdio.h>

void
lowercase (char s[])
{
  for (int i = 0; s[i] != '\0'; ++i)
    s[i] = (s[i] >= 'A' && s[i] <= 'Z') ? s[i] + ('a' - 'A') : s[i];
}

int
main (int   argc,
      char *argv[])
{
  char s[] = "HELLO, world!";

  lowercase (s);
  printf ("%s\n", s);

  return 0;
}
