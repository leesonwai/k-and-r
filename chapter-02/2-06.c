/*
 * Write the function setbits(x,p,n,y) that returns x with the n bits that
 * begin at position p set to the rightmost n bits of y, leaving the other bits
 * unchanged.
 */

#include <stdio.h>

unsigned int
setbits (unsigned int x,
         int          p,
         int          n,
         unsigned int y)
{
  unsigned int mask = ~(~0 << n);

  return (~(mask << (p + 1 - n)) & x) | (mask & y) << (p + 1 - n);
}

int
main (int   argc,
      char *argv[])
{
  printf ("%u\n", setbits (201, 4, 3, 20));

  return 0;
}
