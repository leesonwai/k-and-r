/*
 * Write a program to determine the ranges of char, short, int, and long
 * variables, both signed and unsigned, by printing appropriate values from
 * standard headers and by direct computation. Harder if you compute them:
 * determine the ranges of the various floating-point types.
 */

#include <float.h>
#include <limits.h> 
#include <stdio.h>

int
main (int   argc,
      char *argv[])
{
  printf ("Value of CHAR_MAX: %d\n", CHAR_MAX);
  printf ("Value of SHRT_MAX: %d\n", SHRT_MAX);
  printf ("Value of INT_MAX: %d\n", INT_MAX);
  printf ("Value of LONG_MAX: %ld\n", LONG_MAX);
  printf ("Value of FLT_MAX: %e\n", FLT_MAX);
  printf ("Value of DBL_MAX: %e\n", DBL_MIN);

  return 0;
}
