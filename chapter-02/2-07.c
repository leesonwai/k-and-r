/*
 * Write a function invert(x,p,n) that returns x with the n bits that begin at
 * position p inverted (i.e. 1 changed into 0 and vice versa), leaving the
 * others unchanged.
 */

#include <stdio.h>

unsigned int
invert (unsigned int x,
        int          p,
        int          n)
{
  unsigned int mask = ~(~0 << n);

  return (mask << (p + 1 - n)) ^ x;
}

int
main (int   argc,
      char *argv[])
{
  printf ("%u\n", invert (105, 4, 3));

  return 0;
}
