/*
 * Write an alternate version of squeeze(s1,s2), that deletes each character
 * in s1 that matches any character in the string s2.
 */

#include <stdio.h>

void
squeeze (char s1[],
         char s2[])
{
  int i = 0;
  int j = 0;
  int k = 0;

  while (s1[i] != '\0')
    {
      while (s2[j] != '\0')
        {
          if (s1[i] == s2[j])
            {
              ++i;
              j = 0;
            }
          else
            {
              ++j;
            }
        }
      s1[k++] = s1[i++];
      j = 0;
    }
  s1[k] = '\0';
}

int
main (int   argc,
      char *argv[])
{
  char s1[] = "sHelzqlbio, vfwjokrldu!";
  char s2[] = "kujifsqvabzq";

  squeeze (s1, s2);

  return 0;
}
