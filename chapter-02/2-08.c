/*
 * Write a function rightrot(x,n) that returns the value of the integer x
 * rotated to the right by n bit positions.
 */

#include <stdio.h>

unsigned int
rightrot (unsigned int x,
          int          n)
{
  unsigned int mask = ~(~0 << n);

  return ((mask & x) << ((sizeof (x) * 8) - n)) | (x >> n);
}

int
main (int   argc,
      char *argv[])
{
  printf ("%u\n", rightrot (1, 1));

  return 0;
}
