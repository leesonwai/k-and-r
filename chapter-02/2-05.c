/*
 * Write the function any(s1,s2), which returns the first location in the
 * string s1 where any character from the string s2 occurs, or -1 if s1
 * contains no characters from s2. (The standard library function strpbrk does
 * the same job but returns a pointer to the location.)
 */

#include <stdio.h>

int
any (char s1[],
     char s2[])
{
  int i = 0;
  int j = 0;

  while (s1[i] != '\0')
    {
      while (s2[j] != '\0')
        {
          if (s1[i] == s2[j])
            return i;
          else
            ++j;
        }
      ++i;
      j = 0;
    }

  return -1;
}

int
main (int   argc,
      char *argv[])
{
  char s1[] = "Hello, world!";
  char s2[] = "qzkglep";

  printf ("%d\n", any (s1, s2));

  return 0;
}
