/*
 * Rewrite the postfix calculator of Chapter 4 to use scanf and/or sscanf to do
 * the input and number conversions.
 */

#include <ctype.h>
#include <stdio.h>

#define BUFSIZE 128
#define MAXOP 128
#define MAXSTACK 128
#define NUM '0'

char buf[BUFSIZE];
int bufp;
int sp;
double stack[MAXSTACK];

void
ungetch (int c)
{
  if (bufp >= BUFSIZE)
    printf ("error: too many characters\n");
  else
    buf[bufp++] = c;
}

int
getch ()
{
  return (bufp > 0) ? buf[--bufp] : getchar ();
}

double
pop ()
{
  if (sp > 0)
    {
      return stack[--sp];
    }
  else
    {
      printf ("error: stack empty\n");
      return 0.0;
    }
}

void
push (double f)
{
  if (sp < MAXSTACK)
    stack[sp++] = f;
  else
    printf ("error: stack full, can't push\n");
}

int
getop (char *s)
{
  int c;

  while ((*s = c = getch ()) == ' ' || c == '\t')
    ;
  *++s = '\0';

  if (!isdigit (c) && c != '.')
    return c;
  if (isdigit (c))
    {
      while (isdigit (*s++ = c = getch ()))
        ;
    }
  if (c == '.')
    {
      while (isdigit (*s++ = c = getch ()))
        ;
    }
  *s = '\0';

  if (c != EOF)
    ungetch (c);

  return NUM;
}

int
main (int   argc,
      char *argv[])
{
  int type;
  char s[MAXOP];
  double op;
  double op2;

  while ((type = getop (s)) != EOF)
    {
      switch (type)
        {
        case NUM:
          sscanf (s, "%lf", &op);
          push (op);
          break;
        case '+':
          push (pop () + pop ());
          break;
        case '-':
          op2 = pop ();
          push (pop () - op2);
          break;
        case '*':
          push (pop () * pop ());
          break;
        case '/':
          op2 = pop ();
          if (op2 == 0.0)
            printf ("error: zero divisor\n");
          else
            push (pop () / op2);
          break;
        case '%':
          op2 = pop ();
          push ((int) pop () % (int) op2);
          break;
        case '\n':
          printf ("%.8g\n", pop ());
          break;
        default:
          printf ("error: unknown command\n");
          break;
        }
    }

  return 0;
}
