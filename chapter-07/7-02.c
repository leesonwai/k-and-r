/*
 * Write a program that will print arbitrary input in a sensible way. At a
 * minimum, it should print non-graphic characters in octal or hexadecimal
 * according to local custom, and break long text lines.
 */

#include <ctype.h>
#include <stdio.h>
#include <string.h>

#define LIM 80
#define MAX 8

int
main (int   argc,
      char *argv[])
{
  int c;
  char tmp[MAX];
  int col = 1;

  while ((c = getchar ()) != EOF)
    {
      if (iscntrl (c) || isblank (c))
        {
          sprintf (tmp, "0x%x", c);
          col += strlen (tmp);
          printf ("%s", tmp);
        }
      else
        {
          putchar (c);
        }

      if (col >= LIM)
        {
          putchar ('\n');
          col = 1;
        }
      else
        {
          ++col;
        }
    }
  putchar ('\n');

  return 0;
}
