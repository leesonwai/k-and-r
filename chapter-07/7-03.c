/*
 * Revise minprintf to handle more of the other facilities of printf.
 */

#include <math.h>
#include <stdarg.h>
#include <stdio.h>

void
minprintf (char *fmt,
           ...)
{
  va_list ap;
  double d;
  int i;
  char *s;

  va_start (ap, fmt);

  for (char *p = fmt; *p; ++p)
    {
      if (*p != '%')
        {
          putchar (*p);
          continue;
        }

      switch (*++p)
        {
        case 'c':
          i = va_arg (ap, int);
          putchar (i);
          break;
        case 'd':
          d = va_arg (ap, double);
          printf ("%f", d);
          break;
        case 'g':
          d = va_arg (ap, double);
          printf ("%g", d);
          break;
        case 'i':
          i = va_arg (ap, int);
          printf ("%d", i);
          break;
        case 'o':
          i = va_arg (ap, int);
          printf ("%o", i);
          break;
        case 's':
          for (s = va_arg (ap, char *); *s; ++s)
            putchar (*s);
          break;
        case 'x':
          i = va_arg (ap, int);
          printf ("%x", i);
          break;
        default:
          putchar (*p);
          break;
        }
    }

  va_end (ap);
}

int
main (int   argc,
      char *argv[])
{
  minprintf ("%s = %g\n", "π", M_PI);

  return 0;
}
