/*
 * Write a version of scanf analogous to minprintf from the previous section.
 */

#include <stdarg.h>
#include <stdio.h>

void
minscanf (char *fmt,
          ...)
{
  va_list ap;

  va_start (ap, fmt);

  for (char *p = fmt; *p; ++p)
    {
      if (*p != '%')
        continue;

      switch (*++p)
        {
        case 'c':
          scanf ("%c", va_arg (ap, char *));
          break;
        case 'd':
          scanf ("%d", va_arg (ap, int *));
          break;
        case 'e':
        case 'f':
        case 'g':
          scanf ("%f", va_arg (ap, float *));
          break;
        case 'o':
          scanf ("%o", va_arg (ap, int *));
          break;
        case 's':
          scanf ("%s", va_arg (ap, char *));
          break;
        case 'u':
          scanf ("%u", va_arg (ap, unsigned int *));
          break;
        case 'x':
          scanf ("%x", va_arg (ap, int *));
          break;
        default:
          break;
        }
    }

  va_end (ap);
}

int
main (int   argc,
      char *argv[])
{
  float pi;

  printf ("Enter π: ");
  minscanf ("%g", &pi);
  printf ("π = %g\n", pi);

  return 0;
}
