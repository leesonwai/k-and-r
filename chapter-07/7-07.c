/*
 * Modify the pattern finding program of Chapter 5 to take its input from a set
 * of named files or, if no files are named as arguments, from the standard
 * input. Should the file name be printed when a matching line is found?
 */

#include <stdio.h>
#include <string.h>

#define MAX 128

int
main (int   argc,
      char *argv[])
{
  char *needle;
  char line[MAX];
  FILE *fp;
  char *prog = *argv;

  if (argc < 2)
    {
      fprintf (stderr, "Usage: 'NEEDLE' [FILE]...\n");
      return 1;
    }

  needle = *++argv;

  if (argc == 2)
    {
      while (fgets (line, MAX, stdin) != NULL)
        {
          if (strstr (line, needle))
            printf ("%s", line);
        }
    }

  while (--argc > 1)
    {
      if ((fp = fopen (*++argv, "r")) == NULL)
        {
          fprintf (stderr, "%s: error opening file '%s'\n", prog, *argv);
          continue;
        }

      for (int ln = 1; fgets (line, MAX, fp) != NULL; ++ln)
        {
          if (strstr (line, needle))
            printf ("%s:%d: %s", *argv, ln, line);
        }
      fclose (fp);
    }

  return 0;
}
