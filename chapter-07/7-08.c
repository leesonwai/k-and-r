/*
 * Write a program to print a set of files, starting each new one on a new
 * page, with a title and a running page count for each file.
 */

#include <stdio.h>

int
main (int   argc,
      char *argv[])
{
  FILE *fp;
  char *prog = *argv;
  int pn = 1;
  int c;

  if (argc == 1)
    {
      fprintf (stderr, "Usage: FILE...\n");
      return 1;
    }

  while (--argc)
    {
      if ((fp = fopen (*++argv, "r")) == NULL)
        {
          fprintf (stderr, "%s: error opening file '%s'\n", prog, *argv);
          continue;
        }

      printf ("\n\t%d: %s\n\n", pn++, *argv);
      while ((c = getc (fp)) != EOF)
        putc (c, stdout);
      fclose (fp);
    }

  return 0;
}
