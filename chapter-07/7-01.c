/*
 * Write the program that converts upper case to lower or lower case to upper,
 * depending on the name it is invoked with, as found in argv[0].
 */

#include <ctype.h>
#include <stdio.h>
#include <string.h>

int
main (int   argc,
      char *argv[])
{
  int c;

  if (argc > 1)
    return 1;

  if (strcmp (*argv, "./lower") == 0)
    {
      while ((c = getchar ()) != EOF)
        putchar (tolower (c));
    }
  else if (strcmp (*argv, "./upper") == 0)
    {
      while ((c = getchar ()) != EOF)
        putchar (toupper (c));
    }
  else
    {
      return 1;
    }

  return 0;
}
