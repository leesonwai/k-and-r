/*
 * Write a program to compare to files, printing the first line where they
 * differ.
 */

#include <stdio.h>
#include <string.h>

#define MAX 128

int
main (int   argc,
      char *argv[])
{
  FILE *f;
  FILE *g;
  char s[MAX];
  char t[MAX];
  int line = 1;

  if (argc != 3)
    {
      fprintf (stderr, "Usage: FILE FILE\n");
      return 1;
    }
  else if ((f = fopen (argv[1], "r")) == NULL)
    {
      fprintf (stderr, "%s: error opening file '%s'\n", argv[0], argv[1]);
      return 1;
    }
  else if ((g = fopen (argv[2], "r")) == NULL)
    {
      fprintf (stderr, "%s: error opening file '%s'\n", argv[0], argv[2]);
      return 1;
    }

  while (fgets (s, MAX, f) != NULL && fgets (t, MAX, g) != NULL)
    {
      if (strcmp (s, t) != 0)
        {
          printf ("%d: %s", line, s);
          break;
        }
      ++line;
    }

  fclose (f);
  fclose (g);

  return 0;
}
