## My K&R solutions

This repository documents my efforts at solving the exercises from the 2nd
edition of The C Programming Language.

I've tried to adhere to the following soft goals:

* Use only the concepts the book has explored up to each respective exercise
* Favour clarity and simplicity over exploring edge-cases

Regularly, I'll revisit my solutions to correct rookie mistakes.

Hopefully other learners will find something useful here.
