/*
 * Write a program that reads a C program and prints in alphabetical order each
 * group of variable names that are identical in the first 6 characters, but
 * different somewhere thereafter. Don't count words within strings and
 * comments. Make 6 a parameter that can be set from the command line.
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFSIZE 32
#define DEFAULT 6
#define LEN(a) sizeof (a) / sizeof (a[0])
#define MAXWORD 32

struct iden {
  char *name;
  struct iden *left;
  struct iden *right;
};

char buf[BUFSIZE];
int bufp;
int nchars;

void
ungetch (int c)
{
  if (bufp >= BUFSIZE)
    printf ("error: too many characters\n");
  else
    buf[bufp++] = c;
}

int
getch ()
{
  return (bufp > 0) ? buf[--bufp] : getchar ();
}

struct iden *
talloc ()
{
  return (struct iden *) malloc (sizeof (struct iden));
}

void
treeprint (struct iden *n)
{
  if (n != NULL)
    {
      treeprint (n->left);
      printf ("%s\n", n->name);
      treeprint (n->right);
    }
}

struct iden *
addtree (struct iden *n,
         char        *w)
{
  if (n == NULL)
    {
      n = talloc ();
      n->name = strdup (w);
      n->left = n->right = NULL;
    }
  else if (strcmp (w, n->name) < 0)
    {
      n->left = addtree (n->left, w);
    }
  else
    {
      n->right = addtree (n->right, w);
    }

  return n;
}

int
istype (char *s)
{
  char *datatypes[] = {"char", "double", "float", "int", "void"};

  for (int i = 0; i < LEN (datatypes); ++i)
    {
      if (strcmp (s, datatypes[i]) == 0)
        return 1;
    }

  return 0;
}

int
getword (char *w,
         int   lim)
{
  int c;
  int nc;
  char *start = w;

  while (isspace (c = getch ()))
    ;
  if (c == '/')
    {
      if ((nc = getch ()) != '*')
        {
          c = nc;
        }
      else
        {
          while ((c = getch ()) != '/')
            ;
        }
    }
  if (c == '"')
    {
      while ((c = getch ()) != '"')
        ;
    }
  if (c == '*')
    {
      while ((c = getch ()) == '*')
        ;
    }
  if (c != EOF)
    *w++ = c;
  if (!isalpha (c) && c != '_')
    {
      *w = '\0';
      return c;
    }

  while (--lim)
    {
      if (!isalnum (*w = getch ()) && *w != '_')
        {
          ungetch (*w);
          break;
        }
      ++w;
    }
  *w = '\0';

  return (w - start);
}

int
main (int   argc,
      char *argv[])
{
  char word[MAXWORD];
  struct iden *root = NULL;

  if (--argc && **++argv == '-')
    nchars = abs (atoi (*argv));
  else
    nchars = DEFAULT;

  while (getword (word, MAXWORD) != EOF)
    {
      if (istype (word) && getword (word, MAXWORD) > nchars &&
          (isalpha (word[0]) ||word[0] == '_'))
        root = addtree (root, word);
    }
  treeprint (root);

  return 0;
}
