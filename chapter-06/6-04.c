/*
 * Write a program that prints the distinct words in its input sorted into
 * decreasing order of fequency of occurrence. Precede each word by its count.
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFSIZE 32
#define MAXWORD 32
#define MAXWORDS 4096

enum {
  COUNT,
  W,
};

struct word {
  char *w;
  int count;
};

char buf[BUFSIZE];
int bufp;

void
swap (void *v[],
      int   i,
      int   j)
{
  void *tmp;

  tmp = v[i];
  v[i] = v[j];
  v[j] = tmp;
}

void
ungetch (int c)
{
  if (bufp >= BUFSIZE)
    printf ("error: too many characters\n");
  else
    buf[bufp++] = c;
}

int
getch ()
{
  return (bufp > 0) ? buf[--bufp] : getchar ();
}

struct word *
talloc ()
{
  return (struct word *) malloc (sizeof (struct word));
}

void
quicksort (struct word *words[],
           int          member,
           int          left,
           int          right)
{
  int pivot;
  int last;

  if (left >= right)
    return;

  pivot = (left + right) / 2;
  swap ((void **) words, left, pivot);
  last = left;

  for (int i = left + 1; i <= right; ++i)
    {
      switch (member)
        {
        case COUNT:
          if (words[i]->count > words[left]->count)
            swap ((void **) words, ++last, i);
          break;
        case W:
          if (strcmp (words[i]->w, words[left]->w) < 0)
            swap ((void **) words, ++last, i);
          break;
        default:
          break;
        }
    }

  swap ((void **) words, left, last);
  quicksort (words, member, left, last - 1);
  quicksort (words, member, last + 1, right);
}

struct word *
addstruct (struct word *n,
           char        *w)
{
  n = talloc ();
  n->w = strdup (w);
  n->count = 1;

  return n;
}

struct word *
binsearch (char        *w,
           struct word *words[],
           int          n)
{
  int low = 0;
  int high = n;
  int mid;
  int cond;

  while (low < high)
    {
      mid = (low + high) / 2;
      if ((cond = strcmp (w, words[mid]->w)) < 0)
        high = mid;
      else if (cond > 0)
        low = mid + 1;
      else
        return words[mid];
    }

  return NULL;
}

int
getword (char *w,
         int   lim)
{
  int c;

  while (isspace (c = tolower (getch ())))
    ;
  if (c != EOF)
    *w++ = c;
  if (!isalpha (c))
    {
      *w = '\0';
      return c;
    }

  while (--lim)
    {
      if (!isalpha (*w = tolower (getch ())) && *w != '\'' && *w != '-')
        {
          ungetch (*w);
          break;
        }
      ++w;
    }
  *w = '\0';

  return *w;
}

int
main (int   argc,
      char *argv[])
{
  char w[MAXWORD];
  struct word *tmp = NULL;
  struct word *words[MAXWORDS];
  int nwords = 0;

  while (getword (w, MAXWORD) != EOF && nwords < MAXWORDS)
    {
      if (isalpha (w[0]))
        {
          if ((tmp = binsearch (w, words, nwords)) == NULL)
            words[nwords++] = addstruct (tmp, w);
          else
            ++tmp->count;
          quicksort (words, W, 0, nwords - 1);
        }
    }
  quicksort (words, COUNT, 0, nwords - 1);

  for (int i = 0; i < nwords; ++i)
    printf ("%d: %s\n", words[i]->count, words[i]->w);

  return 0;
}
