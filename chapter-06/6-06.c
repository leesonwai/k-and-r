/*
 * Implement a simple version of the #define processor (i.e., no arguments)
 * suitable for use with C programs, based on the routines of this section. You
 * may also find getch and ungetch helpful.
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFSIZE 32
#define HASHSIZE 256
#define MAXWORD 32

struct nlist {
  struct nlist *next;
  char *name;
  char *defn;
};

char buf[BUFSIZE];
int bufp;
struct nlist *table[HASHSIZE];

unsigned int
hash (char *s)
{
  unsigned int hashval;

  for (hashval = 0; *s; ++s)
    hashval = *s + 31 * hashval;

  return hashval % HASHSIZE;
}

void
ungetch (int c)
{
  if (bufp >= BUFSIZE)
    printf ("error: too many characters\n");
  else
    buf[bufp++] = c;
}

int
getch ()
{
  return (bufp > 0) ? buf[--bufp] : getchar ();
}

struct nlist *
lookup (char *name)
{
  struct nlist *np;

  for (np = table[hash (name)]; np != NULL; np = np->next)
    {
      if (strcmp (name, np->name) == 0)
        return np;
    }

  return NULL;
}

struct nlist *
install (char *name,
         char *defn)
{
  struct nlist *np;
  unsigned int hashval;

  if ((np = lookup (name)) == NULL)
    {
      np = (struct nlist *) malloc (sizeof (struct nlist));
      if (np == NULL || (np->name = strdup (name)) == NULL)
        return NULL;

      hashval = hash (name);
      np->next = table[hashval];
      table[hashval] = np;
    }
  else
    {
      free (np->defn);
    }
  if ((np->defn = strdup (defn)) == NULL)
    return NULL;

  return np;
}

int
getword (char *word,
         int   lim)
{
  int c;
  char *w = word;

  if ((c = getch ()) != EOF)
    *w++ = c;
  if (!isalnum (c) && c != '\'' && c != '#')
    {
      *w = '\0';
      return c;
    }

  while (--lim)
    {
      if (!isalnum (*w = getch ()) && *w != '\'' && *w != '#')
        {
          ungetch (*w);
          break;
        }
      ++w;
    }
  *w = '\0';

  return *word;
}

int
main (int   argc,
      char *argv[])
{
  char word[MAXWORD];
  char *name;
  char *defn;
  struct nlist *np;

  while (getword (word, MAXWORD) != EOF)
    {
      if (strcmp (word, "#define") == 0)
        {
          while (isspace (getword (word, MAXWORD)))
            ;
          name = strdup (word);
          while (isspace (getword (word, MAXWORD)))
            ;
          defn = strdup (word);
          install (name, defn);
          free (name);
          free (defn);
        }
      else
        {
          if (isalnum (word[0]) && (np = lookup (word)) != NULL)
            printf ("%s", np->defn);
          else
            printf ("%s", word);
        }
    }

  return 0;
}
