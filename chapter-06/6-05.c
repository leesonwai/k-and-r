/*
 * Write a function undef that will remove a name and definition from the table
 * maintained by lookup and install.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define HASHSIZE 64

struct nlist * install (char *, char *);
struct nlist * lookup (char *);

struct nlist {
  struct nlist *next;
  char *name;
  char *defn;
};

struct nlist *table[HASHSIZE];

unsigned int
hash (char *s)
{
  unsigned int hashval;

  for (hashval = 0; *s; ++s)
    hashval = *s + 31 * hashval;

  return hashval % HASHSIZE;
}

int
undef (char *name)
{
  struct nlist *curr;
  struct nlist *prev;

  if ((curr = lookup (name)) != NULL)
    {
      for (prev = table[hash (name)]; curr->next != NULL; prev = prev->next)
        {
          if (prev->next == curr)
            {
              prev->next = curr->next;
              break;
            }
        }
      free (curr->name);
      free (curr->defn);
      free (curr);

      return 0;
    }

  return 1;
}

struct nlist *
lookup (char *name)
{
  struct nlist *np;

  for (np = table[hash (name)]; np != NULL; np = np->next)
    {
      if (strcmp (name, np->name) == 0)
        return np;
    }

  return NULL;
}

struct nlist *
install (char *name,
         char *defn)
{
  struct nlist *np;
  unsigned int hashval;

  if ((np = lookup (name)) == NULL)
    {
      np = (struct nlist *) malloc (sizeof (struct nlist));
      if (np == NULL || (np->name = strdup (name)) == NULL)
        return NULL;

      hashval = hash (name);
      np->next = table[hashval];
      table[hashval] = np;
    }
  else
    {
      free (np->defn);
    }
  if ((np->defn = strdup (defn)) == NULL)
    return NULL;

  return np;
}

int
main (int   argc,
      char *argv[])
{
  struct nlist *np;

  install ("HASHSIZE", "64");
  if ((np = lookup ("HASHSIZE")) != NULL)
    printf ("HASHSIZE found in table\n");

  undef ("HASHSIZE");
  if ((np = lookup ("HASHSIZE")) == NULL)
    printf ("HASHSIZE removed from table\n");

  return 0;
}
