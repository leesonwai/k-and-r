/*
 * Write a cross-referencer that prints a list of all words in a document, and,
 * for each word, a list of the line numbers on which it occurs. Remove noise
 * words like "the," "and," and so on.
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFSIZE 32
#define LEN(a) sizeof (a) / sizeof (a[0])
#define MAXLINES 1024
#define MAXWORD 32

struct word {
  char *w;
  int i;
  int lines[MAXLINES];
  struct word *left;
  struct word *right;
};

char buf[BUFSIZE];
int bufp;

void
ungetch (int c)
{
  if (bufp >= BUFSIZE)
    printf ("error: too many characters\n");
  else
    buf[bufp++] = c;
}

int
getch ()
{
  return (bufp > 0) ? buf[--bufp] : getchar ();
}

struct word *
talloc ()
{
  return (struct word *) malloc (sizeof (struct word));
}

void
treeprint (struct word *n)
{
  if (n != NULL)
    {
      treeprint (n->left);
      printf ("%s ", n->w);
      for (int i = 0; i < n->i; ++i)
        printf ("%d%s", n->lines[i], (n->lines[i + 1] == 0) ? "\n" : ", ");
      treeprint (n->right);
    }
}

struct word *
addtree (struct word *n,
         char        *w,
         int          line)
{
  int cond;

  if (n == NULL)
    {
      n = talloc ();
      n->w = strdup (w);
      n->lines[n->i++] = line;
      n->left = n->right = NULL;
    }
  else if ((cond = strcmp (w, n->w)) < 0)
    {
      n->left = addtree (n->left, w, line);
    }
  else if (cond > 0)
    {
      n->right = addtree (n->right, w, line);
    }
  else if (cond == 0)
    {
      n->lines[n->i++] = line;
    }

  return n;
}

int
isnoise (char *w)
{
  char *noise[] = {
    "a", "an", "am", "are", "at", "and", "do", "how", "i", "in", "is", "it",
    "no", "not", "on", "our", "the", "them", "they", "than", "that", "then",
    "there", "these", "this", "those", "to", "too", "us", "we", "were", "what",
    "where", "which", "who", "why", "yes", "you"
  };

  for (int i = 0; i < LEN (noise); ++i)
    {
      if (strcmp (w, noise[i]) == 0)
        return 1;
    }

  return 0;
}

int
getword (char *w,
         int   lim)
{
  int c;

  while (isblank (c = getch ()) || c == '\t')
    ;
  if (c != EOF)
    *w++ = c;
  if (!isalpha (c))
    {
      *w = '\0';
      return c;
    }

  while (--lim)
    {
      if (!isalpha (*w = getch ()) && *w != '\'')
        {
          ungetch (*w);
          break;
        }
      ++w;
    }
  *w = '\0';

  return *w;
}

int
main (int   argc,
      char *argv[])
{
  char w[MAXWORD];
  struct word *root = NULL;
  int line = 1;

  while (getword (w, MAXWORD) != EOF)
    {
      if (isalpha (w[0]) && !isnoise (w))
        root = addtree (root, w, line);
      else if (w[0] == '\n')
        ++line;
    }
  treeprint (root);

  return 0;
}
