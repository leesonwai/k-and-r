/*
 * Our version of getword does not properly handle underscores, string
 * constants, comments, or preprocessor control lines. Write a better version.
 */

#include <ctype.h>
#include <stdio.h>

#define BUFSIZE 32
#define MAXWORD 32

char buf[BUFSIZE];
int bufp;

void
ungetch (int c)
{
  if (bufp >= BUFSIZE)
    printf ("error: too many characters\n");
  else
    buf[bufp++] = c;
}

int
getch ()
{
  return (bufp > 0) ? buf[--bufp] : getchar ();
}

int
getword (char *word,
         int   lim)
{
  int c;
  int nc;
  char *w = word;

  while (isspace (c = getch ()))
    ;
  if (c == '/')
    {
      if ((nc = getch ()) != '*')
        {
          c = nc;
        }
      else
        {
          while ((c = getch ()) != '/')
            ;
        }
    }
  if (c == '"')
    {
      while ((c = getch ()) != '"')
        ;
    }
  if (c == '*')
    {
      while ((c = getch ()) == '*')
        ;
    }
  if (c != EOF)
    *w++ = c;
  if (!isalpha (c) && c != '_' && c != '#')
    {
      *w = '\0';
      return c;
    }

  while (--lim)
    {
      if (!isalnum (*w = getch ()) && *w != '_' && *w != '.')
        {
          ungetch (*w);
          break;
        }
      ++w;
    }
  *w = '\0';

  return *word;
}

int
main (int   argc,
      char *argv[])
{
  char word[MAXWORD];

  while (getword (word, MAXWORD) != EOF)
    {
      if (isalpha (word[0]) || word[0] == '_' || word[0] == '#')
        printf ("%s\n", word);
    }

  return 0;
}
