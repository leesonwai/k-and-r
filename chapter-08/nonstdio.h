#define BUFSIZ 1024
#define EOF (-1)
#define OPEN_MAX 20

typedef struct _iobuf
{
  char *base;
  int cnt;
  int fd;
  char *ptr;

  struct
  {
    unsigned int eof : 1;
    unsigned int err : 1;
    unsigned int rd : 1;
    unsigned int wr : 1;
    unsigned int unbuf : 1;
  } flags;
} FILE;

extern FILE _iob[OPEN_MAX];

#define stdin (&_iob[0])
#define stdout (&_iob[1])
#define stderr (&_iob[2])

int _fillbuf (FILE *);
int _flushbuf (int, FILE *);

#define feof(fp) ((fp)->flags.eof)
#define ferror(fp) ((fp)->flags.err)
#define fileno(fp) ((fp)->fd)

#define getc(fp) (--(fp)->cnt >= 0) ? (unsigned char) *(fp)->ptr++ : _fillbuf (fp)
#define putc(c,fp) (--(fp)->cnt >= 0) ? *(fp)->ptr++ = (c) : _flushbuf((c), (fp))

#define getchar() getc (stdin)
#define putchar(c) putc (c, stdout)
