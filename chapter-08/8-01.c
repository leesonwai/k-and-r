/*
 * Rewrite the program cat from Chapter 7 using read, write, open and close
 * instead of their standard library equivalent. Perform experiments to
 * determine the relative speeds of the two versions.
 */

#include <fcntl.h>
#include <unistd.h>

#define BUFSIZE 256

int
main (int   argc,
      char *argv[])
{
  int n;
  char buf[BUFSIZE];
  int fd;

  if (argc == 1)
    {
      while ((n = read (0, buf, BUFSIZE)) > 0)
        write (1, buf, n);
    }

  while (--argc)
    {
      if ((fd = open (*++argv, O_RDONLY, 0)) == -1)
        continue;

      while ((n = read (fd, buf, BUFSIZE)) > 0)
        write (1, buf, n);
      close (fd);
    }

  return 0;
}
