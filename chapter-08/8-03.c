/*
 * Design and write _flushbuf, fflush, and fclose.
 */

#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#include "nonstdio.h"

#define PERMS 0666

FILE _iob[OPEN_MAX] = {
  {(char *) 0, 0, 0, (char *) 0, {0, 0, 1, 0, 0}},
  {(char *) 0, 0, 1, (char *) 0, {0, 0, 0, 1, 0}},
  {(char *) 0, 0, 2, (char *) 0, {0, 0, 0, 1, 1}}
};

int
_flushbuf (int   c,
           FILE *fp)
{
  int bufsize;
  int cnt;

  if (!fp->flags.wr || fp->flags.err)
    return -1;

  bufsize = (fp->flags.unbuf) ? 1 : BUFSIZ;

  if (fp->base == NULL)
    {
      if ((fp->base = (char *) malloc (bufsize)) == NULL)
        return -1;
    }
  else
    {
      cnt = fp->ptr - fp->base;
      write (fp->fd, fp->base, cnt);
    }

  fp->ptr = fp->base;
  *fp->ptr++ = (unsigned char) c;
  fp->cnt = bufsize - 1;

  return 0;
}

int
_fillbuf (FILE *fp)
{
  int bufsize;

  if (fp->flags.rd && (fp->flags.err || fp->flags.eof))
    return EOF;

  bufsize = (fp->flags.unbuf) ? 1 : BUFSIZ;

  if (fp->base == NULL)
    {
      if ((fp->base = (char *) malloc (bufsize)) == NULL)
        return EOF;
    }

  fp->ptr = fp->base;
  fp->cnt = read (fp->fd, fp->ptr, bufsize);
  if (--fp->cnt < 0)
    {
      if (fp->cnt == -1)
        fp->flags.eof = 1;
      else
        fp->flags.err = 1;
      fp->cnt = 0;

      return EOF;
    }

  return (unsigned char) *fp->ptr++;
}

int
fflush (FILE *fp)
{
  int cnt;

  if (fp->base == NULL || fp->flags.rd)
    return -1;

  cnt = fp->ptr - fp->base;
  write (fp->fd, fp->base, cnt);

  fp->ptr = fp->base;
  fp->cnt = (fp->flags.unbuf) ? 1 : BUFSIZ;

  return 0;
}

int
fileclose (FILE *fp)
{
  if (fflush (fp) == -1)
    return -1;

  free (fp->base);
  close (fp->fd);

  return 0;
}

FILE *
fileopen (char *name,
          char *mode)
{
  FILE *fp;
  int fd;

  if (*mode != 'a' && *mode != 'r' && *mode != 'w')
    return NULL;

  for (fp = _iob; fp < _iob + OPEN_MAX; ++fp)
    {
      if (!fp->flags.rd && !fp->flags.wr)
        break;
    }
  if (fp >= _iob + OPEN_MAX)
    return NULL;

  if (*mode == 'w')
    {
      fd = creat (name, PERMS);
    }
  else if (*mode == 'a')
    {
      if ((fd = open (name, O_WRONLY, 0)) == -1)
        fd = creat (name, PERMS);
      lseek (fd, 0, 2);
    }
  else
    {
      fd = open (name, O_RDONLY, 0);
    }
  if (fd == -1)
    return NULL;

  fp->fd = fd;
  fp->cnt = 0;
  fp->base = NULL;
  if (*mode == 'r')
    fp->flags.rd = 1;
  else
    fp->flags.wr = 1;

  return fp;
}

int
main (int   argc,
      char *argv[])
{
  FILE *dest;
  FILE *src;
  int c;

  if (argc != 3)
    {
      write (2, "Usage: DEST SRC\n", 17);
      return 1;
    }

  if ((dest = fileopen (*++argv, "w")) == NULL)
    return 1;
  if ((src = fileopen (*++argv, "r")) == NULL)
    return 1;

  while ((c = getc (src)) != EOF)
    putc (c, dest);
  fileclose (dest);

  return 0;
}
