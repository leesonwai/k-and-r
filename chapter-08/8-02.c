/*
 * Rewrite fopen and _fillbuf with fields instead of explicit bit operations.
 * Compare code size and execution speed.
 */

#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#include "nonstdio.h"

#define PERMS 0666

FILE _iob[OPEN_MAX] = {
  {(char *) 0, 0, 0, (char *) 0, {0, 0, 1, 0, 0}},
  {(char *) 0, 0, 1, (char *) 0, {0, 0, 0, 1, 0}},
  {(char *) 0, 0, 2, (char *) 0, {0, 0, 0, 1, 1}}
};

int
_fillbuf (FILE *fp)
{
  int bufsize;

  if (fp->flags.rd && (fp->flags.err || fp->flags.eof))
    return EOF;

  bufsize = (fp->flags.unbuf) ? 1 : BUFSIZ;

  if (fp->base == NULL)
    {
      if ((fp->base = (char *) malloc (bufsize)) == NULL)
        return EOF;
    }

  fp->ptr = fp->base;
  fp->cnt = read (fp->fd, fp->ptr, bufsize);
  if (--fp->cnt < 0)
    {
      if (fp->cnt == -1)
        fp->flags.eof = 1;
      else
        fp->flags.err = 1;
      fp->cnt = 0;

      return EOF;
    }

  return (unsigned char) *fp->ptr++;
}

FILE *
fileopen (char *name,
          char *mode)
{
  FILE *fp;
  int fd;

  if (*mode != 'a' && *mode != 'r' && *mode != 'w')
    return NULL;

  for (fp = _iob; fp < _iob + OPEN_MAX; ++fp)
    {
      if (!fp->flags.rd && !fp->flags.wr)
        break;
    }
  if (fp >= _iob + OPEN_MAX)
    return NULL;

  if (*mode == 'w')
    {
      fd = creat (name, PERMS);
    }
  else if (*mode == 'a')
    {
      if ((fd = open (name, O_WRONLY, 0)) == -1)
        fd = creat (name, PERMS);
      lseek (fd, 0, 2);
    }
  else
    {
      fd = open (name, O_RDONLY, 0);
    }
  if (fd == -1)
    return NULL;

  fp->fd = fd;
  fp->cnt = 0;
  fp->base = NULL;
  if (*mode == 'r')
    fp->flags.rd = 1;
  else
    fp->flags.wr = 1;

  return fp;
}

int
main (int   argc,
      char *argv[])
{
  int c;
  FILE *fp;

  if (argc == 1)
    {
      while ((c = getchar ()) != EOF)
        write (1, &c, 1);
    }

  while (--argc)
    {
      if ((fp = fileopen (*++argv, "r")) == NULL)
        continue;

      while ((c = getc (fp)) != EOF)
        write (1, &c, 1);
      close (fp->fd);
    }

  return 0;
}
